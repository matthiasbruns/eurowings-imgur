package com.imgur.core.domain.usecase

interface NoArgUseCase<out R> {

    suspend fun execute(): R
}
