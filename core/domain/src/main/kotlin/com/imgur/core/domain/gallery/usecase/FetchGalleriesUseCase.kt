package com.imgur.core.domain.gallery.usecase

import com.imgur.core.domain.gallery.model.Gallery
import com.imgur.core.domain.gallery.model.GalleryFilter
import com.imgur.core.domain.gallery.repository.GalleryRepository
import com.imgur.core.domain.usecase.UseCase

class FetchGalleriesUseCase(private val repository: GalleryRepository) : UseCase<GalleryFilter?, List<Gallery>> {

    override suspend fun execute(argument: GalleryFilter?): List<Gallery> =
        repository.fetch(argument ?: GalleryFilter())
}