package com.imgur.core.domain.image.model

data class Media(
    val id: String,
    val title: String?,
    val description: String?,
    val datetime: Long,
    val type: String,
    val animated: Boolean,
    val url: String
)