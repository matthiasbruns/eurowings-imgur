package com.imgur.core.domain.usecase

interface UseCase<in A, out R> {

    suspend fun execute(argument: A): R
}
