package com.imgur.core.domain.gallery.model

import com.imgur.core.domain.image.model.Media

data class Gallery(
    val id: String,
    val title: String?,
    val description: String?,
    val datetime: Long,
    val cover: Media,
    val images: List<Media>,
    val upvotes: Int,
    val downvotes: Int,
    val score: Int
)