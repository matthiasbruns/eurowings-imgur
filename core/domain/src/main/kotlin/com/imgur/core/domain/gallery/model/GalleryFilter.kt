package com.imgur.core.domain.gallery.model

enum class GalleryFilterSection {
    HOT, TOP, USER
}

enum class GalleryFilterSort {
    VIRAL, TOP, TIME, RISING
}

enum class GalleryFilterWindow {
    DAY, WEEK, MONTH, YEAR, ALL
}

data class GalleryFilter(
    val section: GalleryFilterSection = GalleryFilterSection.HOT,
    val sort: GalleryFilterSort = GalleryFilterSort.VIRAL,
    val window: GalleryFilterWindow = GalleryFilterWindow.DAY,
    val page: Int = 1,
    val showViral: Boolean = true,
    val mature: Boolean = false
)