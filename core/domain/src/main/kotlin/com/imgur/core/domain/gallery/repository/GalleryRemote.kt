package com.imgur.core.domain.gallery.repository

import com.imgur.core.domain.gallery.model.Gallery
import com.imgur.core.domain.gallery.model.GalleryFilter

interface GalleryRemote {

    suspend fun fetch(filter: GalleryFilter): List<Gallery>

}