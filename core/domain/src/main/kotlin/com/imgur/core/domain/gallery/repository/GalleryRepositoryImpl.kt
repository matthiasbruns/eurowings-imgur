package com.imgur.core.domain.gallery.repository

import com.imgur.core.domain.gallery.model.Gallery
import com.imgur.core.domain.gallery.model.GalleryFilter

class GalleryRepositoryImpl(private val remote: GalleryRemote) : GalleryRepository {

    override suspend fun fetch(filter: GalleryFilter): List<Gallery> = remote.fetch(filter)
}