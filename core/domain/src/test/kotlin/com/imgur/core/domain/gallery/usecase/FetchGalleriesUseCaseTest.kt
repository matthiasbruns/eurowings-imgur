package com.imgur.core.domain.gallery.usecase

import com.imgur.core.domain.gallery.repository.GalleryRepository
import com.imgur.core.domain.prototypes.PROTO_GALLERY
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.experimental.runBlocking
import org.amshove.kluent.shouldEqual
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith

@RunWith(JUnitPlatform::class)
class FetchGalleriesUseCaseTest : Spek({
    lateinit var repository: GalleryRepository
    lateinit var useCase: FetchGalleriesUseCase

    fun prepareMocks() {
        repository = mock()
        useCase = FetchGalleriesUseCase(repository)

        runBlocking {
            whenever(repository.fetch(any())).thenReturn(listOf(PROTO_GALLERY))
        }
    }

    describe("FetchGalleriesUseCase") {
        on("execute") {
            it("should return the provided galleries") {
                prepareMocks()

                runBlocking {
                    useCase.execute(null) shouldEqual listOf(PROTO_GALLERY)
                }
            }
        }
    }
})