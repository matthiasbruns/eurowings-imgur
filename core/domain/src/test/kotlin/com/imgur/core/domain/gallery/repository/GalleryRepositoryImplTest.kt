package com.imgur.core.domain.gallery.repository

import com.imgur.core.domain.gallery.model.GalleryFilter
import com.imgur.core.domain.prototypes.PROTO_GALLERY
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import kotlinx.coroutines.experimental.runBlocking
import org.amshove.kluent.shouldEqual
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith

@RunWith(JUnitPlatform::class)
class GalleryRepositoryImplTest : Spek(
    {
        lateinit var remote: GalleryRemote
        lateinit var repository: GalleryRepositoryImpl

        fun prepareMocks() {
            remote = mock()
            repository = GalleryRepositoryImpl(remote)

            runBlocking {
                whenever(remote.fetch(any())).thenReturn(listOf(PROTO_GALLERY))
            }
        }

        describe("GalleryRepositoryImpl") {

            on("fetch()") {

                it("should return the galleries from remote") {
                    prepareMocks()

                    runBlocking {
                        repository.fetch(GalleryFilter()) shouldEqual listOf(PROTO_GALLERY)
                    }
                }
            }
        }
    }
)