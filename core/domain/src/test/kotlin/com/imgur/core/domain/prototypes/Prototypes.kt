package com.imgur.core.domain.prototypes

import com.imgur.core.domain.gallery.model.Gallery
import com.imgur.core.domain.image.model.Media

val PROTO_MEDIA = Media(
    id = "id",
    description = "description",
    title = "title",
    datetime = 1337,
    animated = true,
    type = "type",
    url = "url"
)

val PROTO_GALLERY = Gallery(
    id = "id",
    downvotes = 1336,
    upvotes = 1337,
    score = 666,
    title = "title",
    description = "description",
    cover = PROTO_MEDIA.copy(),
    datetime = 1337,
    images = listOf(PROTO_MEDIA.copy(), PROTO_MEDIA.copy(id = "OTHER"))
)