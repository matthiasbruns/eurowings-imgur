package com.imgur.core.presentation.main.presenter

import com.imgur.core.presentation.base.BasePresenter
import com.imgur.core.presentation.base.BasePresenterImpl
import com.imgur.core.presentation.base.PresenterCoroutineTrait
import com.imgur.core.presentation.main.navigator.MainNavigator
import com.imgur.core.presentation.main.view.MainView

interface MainPresenter : BasePresenter<MainView>, PresenterCoroutineTrait<MainView> {

    fun onGalleryNavItemClick()

    fun onProfileNavItemClick()

    fun onSettingsNavItemClick()
}

class MainPresenterImpl(private val state: MainPresenterState,
                        private val mainNavigator: MainNavigator) : BasePresenterImpl<MainView>(), MainPresenter {

    override fun onStart() {
        super.onStart()

        if (state.isFirstStart) {
            onGalleryNavItemClick()
            state.isFirstStart = false
        }
    }

    override fun onGalleryNavItemClick() {
        mainNavigator.navigateToGallery()
    }

    override fun onProfileNavItemClick() {
        mainNavigator.navigateToProfile()
    }

    override fun onSettingsNavItemClick() {
        mainNavigator.navigateToSettings()
    }
}