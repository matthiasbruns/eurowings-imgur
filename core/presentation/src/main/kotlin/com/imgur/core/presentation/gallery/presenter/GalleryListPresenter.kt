package com.imgur.core.presentation.gallery.presenter

import com.imgur.core.common.coroutine.throttleFirst
import com.imgur.core.common.extentions.log
import com.imgur.core.presentation.base.BasePresenter
import com.imgur.core.presentation.base.BasePresenterImpl
import com.imgur.core.presentation.base.PresenterCoroutineTrait
import com.imgur.core.presentation.gallery.interactor.GalleryListInteractor
import com.imgur.core.presentation.gallery.model.PresentationGallery
import com.imgur.core.presentation.gallery.model.PresentationGalleryFilter
import com.imgur.core.presentation.gallery.navigator.GalleryNavigator
import com.imgur.core.presentation.gallery.view.GalleryDisplayMode
import com.imgur.core.presentation.gallery.view.GalleryListView
import kotlinx.coroutines.experimental.channels.Channel
import kotlinx.coroutines.experimental.channels.consumeEach

interface GalleryListPresenter : BasePresenter<GalleryListView>,
    PresenterCoroutineTrait<GalleryListView> {

    fun onRequestDisplayChange(mode: GalleryDisplayMode)

    fun onDisplayFilterClick()

    fun applyFilter(filter: PresentationGalleryFilter)

    fun onGalleryItemClick(item: PresentationGallery)
}

class GalleryListPresenterImpl(private val state: GalleryListPresenterState,
                               private val interactor: GalleryListInteractor,
                               private val navigator: GalleryNavigator) : BasePresenterImpl<GalleryListView>(),
    GalleryListPresenter {

    private val onRequestDisplayChangeChannel by lazy { Channel<GalleryDisplayMode>() }

    private val onDisplayFilterClickChannel by lazy { Channel<Unit>() }

    private val onGalleryItemClickChannel by lazy { Channel<PresentationGallery>() }

    override fun onCreate() {
        super.onCreate()

        launchBackground {
            onRequestDisplayChangeChannel.throttleFirst().consumeEach { mode ->
                asyncUi {
                    view?.apply {
                        if (state.renderMode != mode) {
                            state.renderMode = mode
                            setRenderMode(mode)
                        }
                    }
                }
            }
        }

        launchBackground {
            onDisplayFilterClickChannel.throttleFirst().consumeEach {
                asyncUi {
                    view?.showFilterView(state.filter)
                }
            }
        }

        launchBackground {
            onGalleryItemClickChannel.throttleFirst().consumeEach {
                asyncUi {
                    navigator.openDetails(it)
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()

        state.galleries?.let {
            if (it.isEmpty()) {
                null
            } else {
                view?.setGalleries(it)
            }
        } ?: loadData()
    }

    internal fun loadData() {
        view?.setLoadingAnimationVisible(true)

        launchBackground {
            try {
                val galleries = interactor.fetch(state.filter)

                // Cache
                state.galleries = galleries

                asyncUi {
                    view?.also { view ->
                        view.setLoadingAnimationVisible(false)
                        if (galleries.isEmpty()) {
                            view.setEmptyViewVisible(true)
                        } else {
                            view.setEmptyViewVisible(false)
                            view.setGalleries(galleries)
                        }
                    }
                }
            } catch (err: Throwable) {
                err.log("Could not load galleries")
                state.galleries = null
                asyncUi {
                    view?.setErrorViewVisible(true)
                }
            }
        }
    }

    override fun onRequestDisplayChange(mode: GalleryDisplayMode) {
        launchBackground {
            onRequestDisplayChangeChannel.send(mode)
        }
    }

    override fun onDisplayFilterClick() {
        launchBackground {
            onDisplayFilterClickChannel.send(Unit)
        }
    }

    override fun applyFilter(filter: PresentationGalleryFilter) {
        state.filter = filter

        loadData()
    }

    override fun onGalleryItemClick(item: PresentationGallery) {
        launchBackground {
            onGalleryItemClickChannel.send(item)
        }
    }
}
