package com.imgur.core.presentation.main.presenter

data class MainPresenterState(var isFirstStart: Boolean = true)
