package com.imgur.core.presentation.gallery.view

enum class GalleryDisplayMode {
    GRID, LIST, STAGGERED
}