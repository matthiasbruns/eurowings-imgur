package com.imgur.core.presentation.base

import com.imgur.core.common.extentions.log
import com.imgur.core.common.log.Logger
import kotlinx.coroutines.experimental.CancellationException
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withContext
import kotlin.coroutines.experimental.CoroutineContext

/**
 * Trait for a presenter which provides various coroutine helper functions
 * for running coroutines either on the UI or background thread.
 *
 * The property [view] represents a view interface which must implement [BaseView].
 */
interface PresenterCoroutineTrait<out VIEW : BaseView> {

    val view: VIEW?

    fun launchUi(block: suspend () -> Unit) =
            view?.uiCoroutineContext.takeContext("launchUi") { context ->
                launch(context) {
                    block.run()
                }
            }

    fun launchBackground(block: suspend () -> Unit) =
            view?.bgCoroutineContext.takeContext("launchBackground") { context ->
                launch(context) {
                    block.run()
                }
            }

    fun <T> asyncUi(block: suspend () -> T) =
            view?.uiCoroutineContext.takeContext("asyncUi") { context ->
                async(context) {
                    block.run()
                }
            }

    fun <T> asyncBackground(block: suspend () -> T) =
            view?.bgCoroutineContext.takeContext("asyncBackground") { context ->
                async(context) {
                    block.run()
                }
            }

    suspend fun <T> withUiContext(block: suspend () -> T) =
            view?.uiCoroutineContext.takeContext("withUiContext") { context ->
                withContext(context = context, block = block)
            }

    suspend fun <T> withBackgroundContext(block: suspend () -> T) =
            view?.bgCoroutineContext.takeContext("withBackgroundContext") { context ->
                withContext(context = context, block = block)
            }

    private inline fun <T> CoroutineContext?.takeContext(functionName: String,
                                                         body: (CoroutineContext) -> T) =
            if (this == null) {
                Logger.w("$functionName was called but view or context is null. Probably a programming error.")
                null
            } else {
                body(this)
            }

    private suspend inline fun <T> (suspend () -> T).run(): T? =
            try {
                this()
            } catch (e: CancellationException) {
                // This is not an error. Is also fired when job was cancelled normally.
                null
            } catch (e: Exception) {
                e.log("Error while running coroutine")
                throw e
            }
}