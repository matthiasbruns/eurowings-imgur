package com.imgur.core.presentation.gallery.mapper

import com.imgur.core.common.mapper.BidirectionalMapper
import com.imgur.core.domain.gallery.model.GalleryFilter
import com.imgur.core.domain.gallery.model.GalleryFilterSection
import com.imgur.core.domain.gallery.model.GalleryFilterSort
import com.imgur.core.domain.gallery.model.GalleryFilterWindow
import com.imgur.core.presentation.gallery.model.PresentationGalleryFilter
import com.imgur.core.presentation.gallery.model.PresentationGalleryFilterSection
import com.imgur.core.presentation.gallery.model.PresentationGalleryFilterSort
import com.imgur.core.presentation.gallery.model.PresentationGalleryFilterWindow

class PresentationGalleryFilterMapper : BidirectionalMapper<GalleryFilter, PresentationGalleryFilter> {

    override fun mapFrom(to: PresentationGalleryFilter) =
        GalleryFilter(
            showViral = to.showViral,
            page = to.page,
            mature = to.mature,
            window = GalleryFilterWindow.valueOf(to.window.name),
            sort = GalleryFilterSort.valueOf(to.sort.name),
            section = GalleryFilterSection.valueOf(to.section.name)
        )

    override fun mapTo(from: GalleryFilter) =
        PresentationGalleryFilter(
            showViral = from.showViral,
            page = from.page,
            mature = from.mature,
            window = PresentationGalleryFilterWindow.valueOf(from.window.name),
            sort = PresentationGalleryFilterSort.valueOf(from.sort.name),
            section = PresentationGalleryFilterSection.valueOf(from.section.name)
        )
}