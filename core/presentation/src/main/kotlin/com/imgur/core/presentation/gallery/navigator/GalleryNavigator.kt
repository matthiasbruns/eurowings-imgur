package com.imgur.core.presentation.gallery.navigator

import com.imgur.core.presentation.gallery.model.PresentationGallery

interface GalleryNavigator {

    fun openDetails(gallery: PresentationGallery)
}