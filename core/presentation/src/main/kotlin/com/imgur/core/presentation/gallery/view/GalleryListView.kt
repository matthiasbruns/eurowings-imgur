package com.imgur.core.presentation.gallery.view

import com.imgur.core.presentation.base.BaseView
import com.imgur.core.presentation.gallery.model.PresentationGallery
import com.imgur.core.presentation.gallery.model.PresentationGalleryFilter

interface GalleryListView : BaseView {

    fun setLoadingAnimationVisible(visible: Boolean)

    fun setErrorViewVisible(visible: Boolean)

    fun setEmptyViewVisible(visible: Boolean)

    fun setGalleries(items: List<PresentationGallery>)

    fun setRenderMode(mode: GalleryDisplayMode)

    suspend fun showFilterView(filter: PresentationGalleryFilter)
}