package com.imgur.core.presentation.main.navigator

/**
 * This navigator should be implemented twice.
 * Once implementation should handle the installed app  and the other the instant app branch.
 */
interface MainNavigator {

    fun navigateToGallery()

    fun navigateToProfile()

    fun navigateToSettings()
}
