package com.imgur.core.presentation.base

import com.imgur.core.common.coroutine.BgCoroutineContextHolder
import com.imgur.core.common.coroutine.UiCoroutineContextHolder

interface BaseView : UiCoroutineContextHolder, BgCoroutineContextHolder
