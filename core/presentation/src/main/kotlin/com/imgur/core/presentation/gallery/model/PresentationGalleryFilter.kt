package com.imgur.core.presentation.gallery.model

enum class PresentationGalleryFilterSection {
    HOT, TOP, USER
}

enum class PresentationGalleryFilterSort {
    VIRAL, TOP, TIME, RISING
}

enum class PresentationGalleryFilterWindow {
    DAY, WEEK, MONTH, YEAR, ALL
}

data class PresentationGalleryFilter(
    val section: PresentationGalleryFilterSection = PresentationGalleryFilterSection.HOT,
    val sort: PresentationGalleryFilterSort = PresentationGalleryFilterSort.VIRAL,
    val window: PresentationGalleryFilterWindow = PresentationGalleryFilterWindow.DAY,
    val page: Int = 0,
    val showViral: Boolean = true,
    val mature: Boolean = false
)