package com.imgur.core.presentation.gallery.model

data class PresentationGallery(
    val id: String,
    val title: String?,
    val description: String?,
    val coverUrl: String,
    val upvotes: Int,
    val downvotes: Int,
    val score: Int
)