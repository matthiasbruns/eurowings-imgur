package com.imgur.core.presentation.gallery.presenter

import com.imgur.core.presentation.gallery.model.PresentationGallery
import com.imgur.core.presentation.gallery.model.PresentationGalleryFilter
import com.imgur.core.presentation.gallery.view.GalleryDisplayMode

data class GalleryListPresenterState(var galleries: List<PresentationGallery>? = null,
                                     var renderMode: GalleryDisplayMode = GalleryDisplayMode.GRID,
                                     var filter: PresentationGalleryFilter = PresentationGalleryFilter())