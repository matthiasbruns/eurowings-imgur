package com.imgur.core.presentation.gallery.interactor

import com.imgur.core.domain.gallery.usecase.FetchGalleriesUseCase
import com.imgur.core.presentation.gallery.mapper.PresentationGalleryFilterMapper
import com.imgur.core.presentation.gallery.mapper.PresentationGalleryMapper
import com.imgur.core.presentation.gallery.model.PresentationGalleryFilter

class GalleryListInteractor(private val fetchGalleriesUseCase: FetchGalleriesUseCase,
                            private val presentationGalleryMapper: PresentationGalleryMapper,
                            private val presentationGalleryFilterMapper: PresentationGalleryFilterMapper) {

    suspend fun fetch(filter: PresentationGalleryFilter) =
        presentationGalleryFilterMapper.mapFrom(filter).let { fetchGalleriesUseCase.execute(it) }
            .map(presentationGalleryMapper::mapTo)

}