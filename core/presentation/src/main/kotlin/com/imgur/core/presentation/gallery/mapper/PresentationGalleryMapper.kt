package com.imgur.core.presentation.gallery.mapper

import com.imgur.core.common.mapper.Mapper
import com.imgur.core.domain.gallery.model.Gallery
import com.imgur.core.presentation.gallery.model.PresentationGallery

class PresentationGalleryMapper : Mapper<Gallery, PresentationGallery> {

    override fun mapTo(from: Gallery) =
        PresentationGallery(
            id = from.id,
            title = from.title,
            description = from.description,
            coverUrl = from.cover.url,
            score = from.score,
            upvotes = from.upvotes,
            downvotes = from.downvotes
        )
}