package com.imgur.core.presentation.base

import kotlinx.coroutines.experimental.channels.ReceiveChannel

interface BasePresenter<VIEW : BaseView> {

    var view: VIEW?

    /**
     * Is called when the Presenter is being created.
     * The view is not ready yet. Use [onStart] to perform UI related tasks.
     */
    fun onCreate()

    /**
     * Is called when Presenter is started and the UI is ready.
     */
    fun onStart()

    fun onStop()

    fun onDestroy()
}

abstract class BasePresenterImpl<VIEW : BaseView> :
        BasePresenter<VIEW> {

    private val channels = mutableListOf<ReceiveChannel<*>>()

    override var view: VIEW? = null

    override fun onCreate() {
    }

    override fun onStart() {
    }

    override fun onStop() {
        channels.forEach { it.cancel() }
        channels.clear()
    }

    override fun onDestroy() {
    }

    fun ReceiveChannel<*>.cancelOnStop() {
        channels.add(this)
    }
}
