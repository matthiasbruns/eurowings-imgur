package com.imgur.core.common.android.permission

interface RequestPermissionResultTrait {

    var permissionResultHandler: RequestPermissionsResultHandler?

    fun onRequestPermissionsResultDelegate(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        permissionResultHandler?.let {
            it(requestCode, permissions, grantResults)
        }
    }
}
