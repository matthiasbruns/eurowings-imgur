package com.imgur.core.common.android.permission

typealias RequestPermissionsResultHandler = (Int, Array<out String>, IntArray) -> Unit
