package com.imgur.core.common.android.extensions

import android.os.Build

inline fun <T> runOnMinSdk(minSdk: Int, body: () -> T): T? =
    if (Build.VERSION.SDK_INT >= minSdk) body() else null

inline fun <T> runOn21Lollipop(body: () -> T): T? =
    runOnMinSdk(Build.VERSION_CODES.LOLLIPOP, body)

inline fun <T> runOn23Marshmallow(body: () -> T): T? =
    runOnMinSdk(Build.VERSION_CODES.M, body)

inline fun <T> runOn24Nougat(body: () -> T): T? =
    runOnMinSdk(Build.VERSION_CODES.N, body)
