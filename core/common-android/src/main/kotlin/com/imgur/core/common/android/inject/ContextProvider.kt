package com.imgur.core.common.android.inject
import android.content.Context
import android.support.v7.app.AppCompatActivity

/**
 * A [Context], except the application context, should *never* be passed to a Katana module or
 * directly injected into a dependency! This can cause memory leaks when the module or dependency is persisted in memory.
 *
 * Instead a Context should be provided by the ContextProvider. It must be ensured that the provider always returns
 * the *current* Context, for instance after an orientation change. Also the returned Context should only be used in
 * local scopes (local variables, lets) and never be stored as a class property, for example. This could again cause
 * memory leaks.
 */
typealias ContextProvider = () -> Context?

typealias ActivityProvider = () -> AppCompatActivity?
