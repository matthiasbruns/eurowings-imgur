package com.imgur.core.common.factory

interface Factory<out T> {

    fun create(): T
}
