package com.imgur.core.common.mapper

interface BidirectionalMapper<FROM, TO> : Mapper<FROM, TO> {

    fun mapFrom(to: TO): FROM
}
