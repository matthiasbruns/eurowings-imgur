package com.imgur.core.common.coroutine

import kotlinx.coroutines.experimental.DefaultDispatcher
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.channels.*
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import kotlin.coroutines.experimental.CoroutineContext

/**
 * Debounces all items till no items are added anymore in the settleTime time window.
 * The LAST item will be published during the time window.
 */
fun <T> Channel<T>.debounce(
    settleTime: Long = 300,
    context: CoroutineContext = DefaultDispatcher
): ReceiveChannel<T> = produce(context) {
    var job: Job? = null
    consumeEach {
        job?.cancel()
        job = launch {
            delay(settleTime)
            channel.send(it)
        }
    }
    job?.join() //waiting for the last debouncing to end
}

/**
 * Throttles all items after the first one during the settleTime.
 * If 0..n items are pushed, every item will reset the settleTimer.
 * Only the first item during this time window will be sent through the channel.
 * After the settleTimer finishes, the next item will be sent and the logic restarts.
 */
fun <T> Channel<T>.throttleFirst(
    settleTime: Long = 500,
    context: CoroutineContext = DefaultDispatcher
): ReceiveChannel<T> = produce(context) {
    var job: Job? = null
    consumeEach {
        if (job == null || job?.isCompleted == true || job?.isCancelled == true) {
            job = launch {
                channel.send(it)
                delay(settleTime)
                job?.cancel()
            }
        }
    }
}

/**
 * Debounces all items till no items are added anymore in the settleTime time window.
 * The LAST item will be published during the time window.
 */
fun <T> BroadcastChannel<T>.debounce(
    settleTime: Long = 300,
    context: CoroutineContext = DefaultDispatcher
): ReceiveChannel<T> = produce(context) {
    var job: Job? = null
    consumeEach {
        job?.cancel()
        job = launch {
            delay(settleTime)
            channel.send(it)
        }
    }
    job?.join() //waiting for the last debouncing to end
}

/**
 * Throttles all items after the first one during the settleTime.
 * If 0..n items are pushed, every item will reset the settleTimer.
 * Only the first item during this time window will be sent through the channel.
 * After the settleTimer finishes, the next item will be sent and the logic restarts.
 */
fun <T> BroadcastChannel<T>.throttleFirst(
    settleTime: Long = 500,
    context: CoroutineContext = DefaultDispatcher
): ReceiveChannel<T> = produce(context) {
    var job: Job? = null
    consumeEach {
        if (job == null || job?.isCompleted == true || job?.isCancelled == true) {
            job = launch {
                channel.send(it)
                delay(settleTime)
                job?.cancel()
            }
        }
    }
}

/**
 * Sends message to channel if SendChannel is both not closed for send and receive.
 * Returns `true` if was sent else false.
 */
suspend fun <T : Any?> SendChannel<T>.trySend(provider: () -> T) =
    if (isClosedForSend || isFull) {
        false
    } else {
        send(provider())
        true
    }

suspend fun SendChannel<Unit>.trySend() = trySend { }

