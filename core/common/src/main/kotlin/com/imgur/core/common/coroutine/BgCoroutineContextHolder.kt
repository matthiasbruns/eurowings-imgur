package com.imgur.core.common.coroutine

import kotlin.coroutines.experimental.CoroutineContext

/**
 * Holds a coroutine context which is bound to a certain lifecycle where background
 * operations (async API calls, calculations, etc.) should be performed.
 */
interface BgCoroutineContextHolder {

    val bgCoroutineContext: CoroutineContext
}
