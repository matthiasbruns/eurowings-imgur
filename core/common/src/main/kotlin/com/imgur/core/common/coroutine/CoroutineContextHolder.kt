package com.imgur.core.common.coroutine

import kotlin.coroutines.experimental.CoroutineContext

/**
 * Holds a coroutine context which is bound to a certain lifecycle.
 *
 * For use cases see `CoroutineActivity` or `CoroutineViewModel` in app module.
 */
interface CoroutineContextHolder {

    val coroutineContext: CoroutineContext
}
