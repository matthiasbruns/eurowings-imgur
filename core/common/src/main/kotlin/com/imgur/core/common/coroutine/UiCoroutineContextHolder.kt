package com.imgur.core.common.coroutine

import kotlin.coroutines.experimental.CoroutineContext

/**
 * Holds a coroutine context which is bound to the lifecycle of the UI.
 * All UI related operations should use this context.
 */
interface UiCoroutineContextHolder {

    var uiCoroutineContext: CoroutineContext?
}
