package com.imgur.core.common.extentions

fun String?.emptyToNull() = if (this?.isEmpty() == true) null else this
