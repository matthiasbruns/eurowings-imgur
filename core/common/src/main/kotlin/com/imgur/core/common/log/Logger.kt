package com.imgur.core.common.log

/**
 * Logger wrapper class to be able to use Android-specific logger libraries in non-Android modules.
 *
 * An implementation of [LoggerAdapter] must be implemented in Android project and applied via the
 * [adapter] property.
 *
 * @see LoggerAdapter
 */
@Suppress("unused", "MemberVisibilityCanBePrivate")
object Logger {

    enum class Level { VERBOSE, DEBUG, INFO, WARN, ERROR }

    var adapter: LoggerAdapter? = SimpleLoggerAdapter()

    fun log(level: Level, throwable: Throwable? = null, message: String? = null, vararg args: Any? = emptyArray()) {
        when (level) {
            Level.VERBOSE -> v(throwable = throwable, message = message, args = *args)
            Level.DEBUG -> d(throwable = throwable, message = message, args = *args)
            Level.INFO -> i(throwable = throwable, message = message, args = *args)
            Level.WARN -> w(throwable = throwable, message = message, args = *args)
            Level.ERROR -> e(throwable = throwable, message = message, args = *args)
        }
    }

    fun v(message: String, vararg args: Any? = emptyArray()) {
        v(throwable = null, message = message, args = *args)
    }

    fun v(throwable: Throwable? = null, message: String? = null, vararg args: Any? = emptyArray()) {
        adapter?.v(throwable = throwable, message = message, args = *args)
    }

    fun d(message: String, vararg args: Any? = emptyArray()) {
        d(throwable = null, message = message, args = *args)
    }

    fun d(throwable: Throwable? = null, message: String? = null, vararg args: Any? = emptyArray()) {
        adapter?.d(throwable = throwable, message = message, args = *args)
    }

    fun i(message: String, vararg args: Any? = emptyArray()) {
        i(throwable = null, message = message, args = *args)
    }

    fun i(throwable: Throwable? = null, message: String? = null, vararg args: Any? = emptyArray()) {
        adapter?.i(throwable = throwable, message = message, args = *args)
    }

    fun w(message: String, vararg args: Any? = emptyArray()) {
        w(throwable = null, message = message, args = *args)
    }

    fun w(throwable: Throwable? = null, message: String? = null, vararg args: Any? = emptyArray()) {
        adapter?.w(throwable = throwable, message = message, args = *args)
    }

    fun e(message: String, vararg args: Any? = emptyArray()) {
        e(throwable = null, message = message, args = *args)
    }

    fun e(throwable: Throwable? = null, message: String? = null, vararg args: Any? = emptyArray()) {
        adapter?.e(throwable = throwable, message = message, args = *args)
    }

    fun tag(tag: String?) {
        adapter?.tag(tag)
    }
}
