package com.imgur.core.common.log

import java.text.SimpleDateFormat
import java.util.*

/**
 * Very simple implementation of [LoggerAdapter] that just logs to console for unit tests for example.
 */
class SimpleLoggerAdapter : LoggerAdapter {

    private val explicitTag = ThreadLocal<String?>()

    override fun v(throwable: Throwable?, message: String?, vararg args: Any?) {
        log(formatMessage(logLevel = "V", throwable = throwable, message = message, args = *args))
    }

    override fun d(throwable: Throwable?, message: String?, vararg args: Any?) {
        log(formatMessage(logLevel = "D", throwable = throwable, message = message, args = *args))
    }

    override fun i(throwable: Throwable?, message: String?, vararg args: Any?) {
        log(formatMessage(logLevel = "I", throwable = throwable, message = message, args = *args))
    }

    override fun w(throwable: Throwable?, message: String?, vararg args: Any?) {
        log(formatMessage(logLevel = "W", throwable = throwable, message = message, args = *args))
    }

    override fun e(throwable: Throwable?, message: String?, vararg args: Any?) {
        log(formatMessage(logLevel = "E", throwable = throwable, message = message, args = *args), isError = true)
    }

    override fun tag(tag: String?) {
        explicitTag.set(tag)
    }

    internal fun formatMessage(date: Date = Date(),
                               logLevel: String,
                               throwable: Throwable? = null,
                               message: String? = null,
                               vararg args: Any? = emptyArray()): String {
        val tag = explicitTag.get()?.apply { explicitTag.remove() }
        return with(StringBuilder()) {
            append(DATE_FORMAT.format(date))
            append(" ")
            append(logLevel)
            if (tag != null) {
                append("/$tag")
            }
            if (message != null) {
                append(": ")
                if (args.isNotEmpty()) {
                    append(String.format(message, *args))
                } else {
                    append(message)
                }
            }
            if (throwable != null) {
                appendln()
                append(throwable.message)
            }
            toString()
        }
    }

    private fun log(message: String, isError: Boolean = false) {
        if (isError) {
            System.err.println(message)
        } else {
            System.out.println(message)
        }
    }

    private companion object {
        private val DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S", Locale.GERMAN)
    }
}
