package com.imgur.core.common.extentions

import com.imgur.core.common.log.Logger
import com.imgur.core.common.log.Logger.Level
import com.imgur.core.common.log.Logger.Level.ERROR

fun Throwable.log(level: Level = ERROR, message: String? = null) =
        Logger.log(level = level, throwable = this, message = message)

fun Throwable.log(message: String? = null) =
        log(level = ERROR, message = message)
