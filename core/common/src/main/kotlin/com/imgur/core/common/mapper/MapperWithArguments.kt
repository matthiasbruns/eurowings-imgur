package com.imgur.core.common.mapper

interface MapperWithArguments<in FROM, out TO, in ARGS> {

    fun mapTo(from: FROM, args: ARGS): TO
}
