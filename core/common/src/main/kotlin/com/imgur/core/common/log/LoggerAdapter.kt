package com.imgur.core.common.log

/**
 * Adapter for wrapping a logger implementation to be used by [Logger].
 *
 * @see Logger
 */
interface LoggerAdapter {

    fun v(throwable: Throwable? = null, message: String? = null, vararg args: Any? = emptyArray())

    fun d(throwable: Throwable? = null, message: String? = null, vararg args: Any? = emptyArray())

    fun i(throwable: Throwable? = null, message: String? = null, vararg args: Any? = emptyArray())

    fun w(throwable: Throwable? = null, message: String? = null, vararg args: Any? = emptyArray())

    fun e(throwable: Throwable? = null, message: String? = null, vararg args: Any? = emptyArray())

    fun tag(tag: String?)
}
