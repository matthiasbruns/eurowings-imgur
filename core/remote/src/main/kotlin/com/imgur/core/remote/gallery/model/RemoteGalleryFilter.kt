package com.imgur.core.remote.gallery.model

data class RemoteGalleryFilter(
    val section: String = "hot",
    val sort: String = "viral",
    val window: String = "day",
    val page: Int = 0,
    val showViral: Boolean = true,
    val mature: Boolean = false
)