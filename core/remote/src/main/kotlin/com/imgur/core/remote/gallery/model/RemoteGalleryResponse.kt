package com.imgur.core.remote.gallery.model

data class RemoteGalleryResponse(
    val data: List<RemoteGallery>?,
    val success: Boolean,
    val status: Int
)