package com.imgur.core.remote.gallery.mapper

import com.imgur.core.common.mapper.Mapper
import com.imgur.core.domain.gallery.model.Gallery
import com.imgur.core.remote.gallery.model.RemoteGallery
import com.imgur.core.remote.image.mapper.RemoteImageMapper

class RemoteGalleryMapper(private val imageMapper: RemoteImageMapper) : Mapper<RemoteGallery, Gallery?> {

    override fun mapTo(from: RemoteGallery) =
        from.images?.let { nonNullImages ->
            nonNullImages.firstOrNull { it.id == from.cover }?.let { imageMapper.mapTo(it) }?.let { cover ->
                Gallery(
                    id = from.id,
                    title = from.title,
                    description = from.description,
                    datetime = from.datetime,
                    images = nonNullImages.map(imageMapper::mapTo),
                    cover = cover,
                    downvotes = from.downs ?: 0,
                    upvotes = from.ups ?: 0,
                    score = from.score ?: 0)
            }
        }
}