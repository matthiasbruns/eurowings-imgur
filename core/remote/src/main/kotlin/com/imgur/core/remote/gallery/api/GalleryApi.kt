package com.imgur.core.remote.gallery.api

import com.imgur.core.remote.gallery.model.RemoteGalleryResponse
import kotlinx.coroutines.experimental.Deferred
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GalleryApi {

    /**
     * https://api.imgur.com/3/gallery/{{section}}/{{sort}}/{{window}}/{{page}}?showViral={{showViral}}&mature={{showMature}}&album_previews={{albumPreviews}}
     */
    @GET("gallery/{section}/{sort}/{window}/{page}")
    fun fetch(@Path("section") section: String,
              @Path("sort") sort: String,
              @Path("window") window: String,
              @Path("page") page: Int,
              @Query("showViral") showViral: Boolean): Deferred<RemoteGalleryResponse>

}