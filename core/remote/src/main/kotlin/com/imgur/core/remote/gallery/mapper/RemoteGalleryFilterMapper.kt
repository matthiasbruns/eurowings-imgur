package com.imgur.core.remote.gallery.mapper

import com.imgur.core.common.mapper.Mapper
import com.imgur.core.domain.gallery.model.GalleryFilter
import com.imgur.core.remote.gallery.model.RemoteGalleryFilter

class RemoteGalleryFilterMapper : Mapper<GalleryFilter, RemoteGalleryFilter> {

    override fun mapTo(from: GalleryFilter) =
        RemoteGalleryFilter(
            section = from.section.name.toLowerCase(),
            sort = from.sort.name.toLowerCase(),
            window = from.window.name.toLowerCase(),
            showViral = from.showViral,
            page = from.page,
            mature = from.mature
        )
}