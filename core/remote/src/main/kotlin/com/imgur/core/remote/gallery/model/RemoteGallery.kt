package com.imgur.core.remote.gallery.model

import com.imgur.core.remote.image.model.RemoteImage

data class RemoteGallery(
    val id: String,
    val title: String?,
    val description: String?,
    val datetime: Long,
    val cover: String,
    val images: List<RemoteImage>?,
    val ups: Int?,
    val downs: Int?,
    val score: Int?
)