package com.imgur.core.remote.gallery.repository

import com.imgur.core.common.log.Logger
import com.imgur.core.domain.gallery.model.Gallery
import com.imgur.core.domain.gallery.model.GalleryFilter
import com.imgur.core.domain.gallery.repository.GalleryRemote
import com.imgur.core.remote.CredentialProvider
import com.imgur.core.remote.gallery.api.GalleryApi
import com.imgur.core.remote.gallery.mapper.RemoteGalleryFilterMapper
import com.imgur.core.remote.gallery.mapper.RemoteGalleryMapper

class GalleryRemoteImpl(private val credentialProvider: CredentialProvider,
                        private val api: GalleryApi,
                        private val mapper: RemoteGalleryMapper,
                        private val filterMapper: RemoteGalleryFilterMapper) : GalleryRemote {

    override suspend fun fetch(filter: GalleryFilter): List<Gallery> =
        filterMapper.mapTo(filter).let { remoteFilter ->
            credentialProvider()?.let {
                api.fetch(section = remoteFilter.section,
                    window = remoteFilter.window,
                    sort = remoteFilter.sort,
                    page = remoteFilter.page,
                    showViral = remoteFilter.showViral).await().data?.mapNotNull(mapper::mapTo)
            } ?: run {
                Logger.w("No credentialsProvider was found")
                return emptyList()
            }
        }

}