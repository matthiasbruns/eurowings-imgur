package com.imgur.core.remote.image.mapper

import com.imgur.core.common.mapper.Mapper
import com.imgur.core.domain.image.model.Media
import com.imgur.core.remote.image.model.RemoteImage

class RemoteImageMapper : Mapper<RemoteImage, Media> {

    override fun mapTo(from: RemoteImage) =
        Media(
            id = from.id,
            title = from.title,
            url = from.link,
            description = from.description,
            type = from.type,
            animated = from.animated,
            datetime = from.datetime
        )
}