package com.imgur.core.remote

import com.imgur.core.remote.gallery.api.GalleryApi
import retrofit2.Retrofit

internal class ApiImpl(private val client: Retrofit) : Api {

    override val gallery: GalleryApi
        get() = client.create(GalleryApi::class.java)
}