package com.imgur.core.remote

import com.imgur.core.remote.gallery.api.GalleryApi

interface Api {
    val gallery: GalleryApi
}