package com.imgur.core.remote

class ApiCredentials(val clienId: String,
                     val clientSecret: String?) {

    val authorization: String
        get() {
            return "Client-ID $clienId"
        }
}

typealias CredentialProvider = () -> ApiCredentials?