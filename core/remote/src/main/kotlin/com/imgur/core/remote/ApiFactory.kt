package com.imgur.core.remote

import com.imgur.core.common.factory.Factory
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class ApiFactory(private val credentialsProvider: CredentialProvider) : Factory<Api> {

    private val okHttpClient: OkHttpClient
        get() = OkHttpClient.Builder().apply {
            addInterceptor { chain ->
                var request = chain.request()

                credentialsProvider()?.let { credentials ->
                    val headers = request.headers().newBuilder().add("Authorization", credentials.authorization).build()
                    request = request.newBuilder().headers(headers).build()
                }

                chain.proceed(request)
            }
        }.build()


    private val retrofit: Retrofit
        get() = Retrofit.Builder().baseUrl("https://api.imgur.com/3/")
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()


    override fun create(): Api = ApiImpl(retrofit)
}