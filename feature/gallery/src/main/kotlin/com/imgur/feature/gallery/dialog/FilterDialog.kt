package com.imgur.feature.gallery.dialog

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.imgur.feature.gallery.R
import com.imgur.feature.gallery.model.ParcelableGalleryFilter
import com.imgur.feature.gallery.model.ParcelableGalleryFilterSection
import com.imgur.feature.gallery.model.ParcelableGalleryFilterSort
import com.imgur.feature.gallery.model.ParcelableGalleryFilterWindow
import kotlinx.android.synthetic.main.gallery_dialog_filter.*
import kotlinx.android.synthetic.main.gallery_dialog_filter.view.*


/*
    TODO: build a presenter for dialogs - but this is out of scope
    There is also a update missing which enables or disables the UI based on the selected section e.g.
  */
class FilterDialog : DialogFragment() {
    var filter: ParcelableGalleryFilter? = null

    var confirmListener: ((filter: ParcelableGalleryFilter) -> Unit)? = null

    private val onItemSelectListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {}

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            when {
                parent === gallery_sectionSpinner -> onSectionSelected(position)
                parent === gallery_windowSpinner -> onWindowsSelected(position)
                parent === gallery_sortSpinner -> onSortSelected(position)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.gallery_dialog_filter, container, false).apply {
            setupSpinner(gallery_sectionSpinner, R.array.gallery_filter_sections)
            setupSpinner(gallery_windowSpinner, R.array.gallery_filter_windows)
            setupSpinner(gallery_sortSpinner, R.array.gallery_filter_sorts)
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupStartingValues()

        gallery_sectionSpinner.onItemSelectedListener = onItemSelectListener
        gallery_windowSpinner.onItemSelectedListener = onItemSelectListener
        gallery_sortSpinner.onItemSelectedListener = onItemSelectListener

        gallery_filterConfirm.setOnClickListener {
            filter?.let { nonNullFilter -> confirmListener?.invoke(nonNullFilter) }
            dismiss()
        }

        gallery_showViralSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            onShowViralChanged(isChecked)
        }
    }

    private fun setupStartingValues() {
        val parcelableGalleryFilter = arguments!!.getParcelable(EXTRA_FILTER) as ParcelableGalleryFilter
        filter = parcelableGalleryFilter

        gallery_sectionSpinner.setSelection(when (parcelableGalleryFilter.section) {
            ParcelableGalleryFilterSection.HOT -> 0
            ParcelableGalleryFilterSection.TOP -> 1
            else -> 2
        })

        gallery_windowSpinner.setSelection(when (parcelableGalleryFilter.window) {
            ParcelableGalleryFilterWindow.DAY -> 0
            ParcelableGalleryFilterWindow.WEEK -> 1
            ParcelableGalleryFilterWindow.MONTH -> 2
            ParcelableGalleryFilterWindow.YEAR -> 3
            ParcelableGalleryFilterWindow.ALL -> 4
        })

        gallery_sortSpinner.setSelection(when (parcelableGalleryFilter.sort) {
            ParcelableGalleryFilterSort.VIRAL -> 0
            ParcelableGalleryFilterSort.TOP -> 1
            ParcelableGalleryFilterSort.TIME -> 2
            else -> 0
        })

        gallery_showViralSwitch.isChecked = parcelableGalleryFilter.showViral
    }

    private fun setupSpinner(spinner: Spinner, arrayResource: Int) {
        context?.also { context ->
            val adapter = ArrayAdapter.createFromResource(context, arrayResource, android.R.layout.simple_spinner_item)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }
    }

    private fun onSectionSelected(index: Int) {
        filter = filter.let {
            (it ?: ParcelableGalleryFilter()).copy(section = when (index) {
                0 -> ParcelableGalleryFilterSection.HOT
                1 -> ParcelableGalleryFilterSection.TOP
                else -> ParcelableGalleryFilterSection.USER
            })
        }

    }

    private fun onSortSelected(index: Int) {
        filter = filter.let {
            (it ?: ParcelableGalleryFilter()).copy(sort = when (index) {
                0 -> ParcelableGalleryFilterSort.VIRAL
                1 -> ParcelableGalleryFilterSort.TOP
                else -> ParcelableGalleryFilterSort.TIME
            })
        }
    }

    private fun onWindowsSelected(index: Int) {
        filter = filter.let {
            (it ?: ParcelableGalleryFilter()).copy(window = when (index) {
                0 -> ParcelableGalleryFilterWindow.DAY
                1 -> ParcelableGalleryFilterWindow.WEEK
                2 -> ParcelableGalleryFilterWindow.MONTH
                3 -> ParcelableGalleryFilterWindow.YEAR
                else -> ParcelableGalleryFilterWindow.ALL
            })
        }
    }

    private fun onShowViralChanged(showViral: Boolean) {
        filter = filter.let {
            (it ?: ParcelableGalleryFilter()).copy(showViral = showViral)
        }
    }

    companion object {
        private const val EXTRA_FILTER = "EXTRA_FILTER"

        fun instance(filter: ParcelableGalleryFilter) =
            FilterDialog().apply {
                arguments = Bundle().apply { putParcelable(EXTRA_FILTER, filter) }
            }
    }
}