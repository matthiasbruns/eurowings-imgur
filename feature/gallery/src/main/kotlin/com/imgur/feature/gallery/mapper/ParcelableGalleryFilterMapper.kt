package com.imgur.feature.gallery.mapper

import com.imgur.core.common.mapper.BidirectionalMapper
import com.imgur.core.presentation.gallery.model.PresentationGalleryFilter
import com.imgur.core.presentation.gallery.model.PresentationGalleryFilterSection
import com.imgur.core.presentation.gallery.model.PresentationGalleryFilterSort
import com.imgur.core.presentation.gallery.model.PresentationGalleryFilterWindow
import com.imgur.feature.gallery.model.ParcelableGalleryFilter
import com.imgur.feature.gallery.model.ParcelableGalleryFilterSection
import com.imgur.feature.gallery.model.ParcelableGalleryFilterSort
import com.imgur.feature.gallery.model.ParcelableGalleryFilterWindow

//TODO: move enum mappers to own classes - not in scope
class ParcelableGalleryFilterMapper : BidirectionalMapper<PresentationGalleryFilter, ParcelableGalleryFilter> {

    override fun mapFrom(to: ParcelableGalleryFilter): PresentationGalleryFilter {
        return PresentationGalleryFilter(
            section = PresentationGalleryFilterSection.valueOf(to.section.name),
            sort = PresentationGalleryFilterSort.valueOf(to.sort.name),
            window = PresentationGalleryFilterWindow.valueOf(to.window.name),
            mature = to.mature,
            page = to.page,
            showViral = to.showViral
        )
    }

    override fun mapTo(from: PresentationGalleryFilter): ParcelableGalleryFilter {
        return ParcelableGalleryFilter(
            section = ParcelableGalleryFilterSection.valueOf(from.section.name),
            sort = ParcelableGalleryFilterSort.valueOf(from.sort.name),
            window = ParcelableGalleryFilterWindow.valueOf(from.window.name),
            mature = from.mature,
            page = from.page,
            showViral = from.showViral
        )
    }
}