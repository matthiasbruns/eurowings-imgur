package com.imgur.feature.gallery.controller

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.ControllerChangeHandler
import com.bluelinelabs.conductor.ControllerChangeType
import com.imgur.feature.base.ImgurApp
import com.imgur.feature.base.glide.GlideApp
import com.imgur.feature.base.glide.withListener
import com.imgur.feature.base.view.StatusBarControllerTrait
import com.imgur.feature.gallery.R
import com.imgur.feature.gallery.createGalleryDetailModule
import com.imgur.feature.gallery.galleryCoreModule
import com.imgur.feature.gallery.mapper.ParcelableGalleryMapper
import com.imgur.feature.gallery.model.ParcelableGallery
import kotlinx.android.synthetic.main.gallery_controller_detail.view.*
import org.rewedigital.katana.KatanaTrait
import org.rewedigital.katana.createComponent
import org.rewedigital.katana.inject

/**
 * Should extend PresenterController but this is out of scope
 */
class GalleryDetailController(bundle: Bundle) : Controller(bundle),
    StatusBarControllerTrait,
    KatanaTrait {

    override val controller: Controller
        get() = this

    override val component =
        createComponent(modules = listOf(createGalleryDetailModule {
            parentController?.router ?: router
        }, galleryCoreModule),
            dependsOn = listOf(ImgurApp.applicationComponent))

    private val gallery
        get() = args.getParcelable<ParcelableGallery>(EXTRA_GALLERY)?.let(mapper::mapFrom)
            ?: throw Error("Coupon not found")

    private val mapper by inject<ParcelableGalleryMapper>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.gallery_controller_detail, container, false).apply {
            gallery.also {
                gallery_itemName.text = it.title
                gallery_itemDescription.text = it.description
                gallery_itemDescription.visibility = if (it.description.isNullOrBlank()) View.GONE else View.VISIBLE
                gallery_upvotes.text = it.upvotes.toString()
                gallery_downvotes.text = it.downvotes.toString()
                gallery_score.text = it.score.toString()

                setProgressBarVisible(true, view)

                GlideApp.with(this)
                    .load(gallery.coverUrl)
                    .withListener {
                        setProgressBarVisible(false, this)
                        false
                    }
                    .into(gallery_coverImage)
            }
        }

    override fun onChangeStarted(changeHandler: ControllerChangeHandler, changeType: ControllerChangeType) {
        super.onChangeStarted(changeHandler, changeType)
        onChangeStartedDelegate(changeHandler, changeType)
    }

    override fun onChangeEnded(changeHandler: ControllerChangeHandler, changeType: ControllerChangeType) {
        onChangeEndedDelegate(changeHandler, changeType)
        super.onChangeEnded(changeHandler, changeType)
    }

    private fun setProgressBarVisible(visible: Boolean, view: View?) {
        view?.apply {
            offer_productImageAnimationView.visibility = if (visible) android.view.View.VISIBLE else android.view.View.GONE
            gallery_coverImage.visibility = if (visible) android.view.View.INVISIBLE else android.view.View.VISIBLE
        }
    }

    companion object {

        private const val EXTRA_GALLERY = "EXTRA_GALLERY"

        fun instance(gallery: ParcelableGallery) =
            GalleryDetailController(Bundle(1).apply {
                putParcelable(EXTRA_GALLERY, gallery)
            })
    }
}