package com.imgur.feature.gallery.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.imgur.core.presentation.gallery.model.PresentationGallery
import com.imgur.core.presentation.gallery.view.GalleryDisplayMode
import com.imgur.feature.base.glide.GlideApp
import com.imgur.feature.base.glide.withListener
import com.imgur.feature.base.util.Conversions
import kotlinx.android.synthetic.main.gallery_item_gallery.view.*

class GalleryViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

    fun bind(gallery: PresentationGallery, mode: GalleryDisplayMode) {
        if (mode == GalleryDisplayMode.GRID) {
            setGridDimens()
        }

        setProgressBarVisible(true)

        with(itemView) {
            gallery_itemName.text = gallery.title
            gallery.description?.also {
                gallery_itemDescription.visibility = View.VISIBLE
                gallery_itemDescription.text = it
            } ?: run { gallery_itemDescription.visibility = View.GONE }

            GlideApp.with(itemView)
                .load(gallery.coverUrl)
                .withListener {
                    setProgressBarVisible(false)
                    false
                }
                .into(gallery_coverImage)
        }
    }

    private fun setGridDimens() {
        val displayMetrics = itemView.resources.displayMetrics
        val gridWidth = displayMetrics.widthPixels.toDouble() / (FULLY_DISPLAYED_GRID_ROWS) - Conversions.dpToPx(2)
        val layoutParams = itemView.layoutParams

        layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        layoutParams.height = (gridWidth * 1.25).toInt()

        itemView.layoutParams = layoutParams
    }

    private fun setProgressBarVisible(visible: Boolean) {
        with(itemView) {
            offer_productImageAnimationView.visibility = if (visible) View.VISIBLE else View.GONE
            gallery_coverImage.visibility = if (visible) View.INVISIBLE else View.VISIBLE
        }
    }

    companion object {
        private const val FULLY_DISPLAYED_GRID_ROWS = 2.0
    }
}