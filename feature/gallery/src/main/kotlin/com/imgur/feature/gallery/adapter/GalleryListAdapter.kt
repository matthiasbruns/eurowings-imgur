package com.imgur.feature.gallery.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.imgur.core.presentation.gallery.model.PresentationGallery
import com.imgur.core.presentation.gallery.view.GalleryDisplayMode
import com.imgur.feature.base.view.AutoUpdatableAdapterTrait
import com.imgur.feature.gallery.R
import com.imgur.feature.gallery.viewholder.GalleryViewHolder
import kotlin.properties.Delegates

class GalleryListAdapter : RecyclerView.Adapter<GalleryViewHolder>(),
    AutoUpdatableAdapterTrait {

    init {
        setHasStableIds(true)
    }

    var mode: GalleryDisplayMode = GalleryDisplayMode.GRID

    var items: List<PresentationGallery> by Delegates.observable(emptyList()) { _, oldList, newList ->
        autoNotify(oldList, newList) { old, new -> old.id == new.id }
    }

    var onGalleryClickListener: ((gallery: PresentationGallery) -> Unit)? = null

    override fun getItemId(position: Int): Long = items[position].id.hashCode().toLong()

    override fun getItemCount() = items.size

    override fun getItemViewType(position: Int): Int =
        when (mode) {
            GalleryDisplayMode.STAGGERED -> VIEW_TYPE_STAGGERED
            else -> VIEW_TYPE_DEFAULT
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryViewHolder =
        GalleryViewHolder(LayoutInflater.from(parent.context).inflate(when (viewType) {
            VIEW_TYPE_STAGGERED -> R.layout.gallery_item_gallery_staggered
            else -> R.layout.gallery_item_gallery
        }, parent, false))

    override fun onBindViewHolder(holder: GalleryViewHolder, position: Int) {
        items[position].apply {
            holder.bind(this, mode)
            holder.itemView.setOnClickListener { onGalleryClickListener?.invoke(this) }
        }
    }

    companion object {
        private val VIEW_TYPE_DEFAULT = 0
        private val VIEW_TYPE_STAGGERED = 1
    }
}