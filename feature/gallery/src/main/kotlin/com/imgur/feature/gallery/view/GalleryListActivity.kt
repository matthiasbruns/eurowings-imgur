package  com.imgur.feature.gallery.view

import com.imgur.feature.base.common.ControllerActivity
import com.imgur.feature.gallery.controller.GalleryListController


class GalleryListActivity : ControllerActivity() {

    override fun provideController() = GalleryListController()
}