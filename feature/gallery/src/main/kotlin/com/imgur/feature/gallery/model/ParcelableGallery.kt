package com.imgur.feature.gallery.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ParcelableGallery(
    val id: String,
    val title: String?,
    val description: String?,
    val coverUrl: String,
    val upvotes: Int,
    val downvotes: Int,
    val score: Int) : Parcelable