package com.imgur.feature.gallery.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

enum class ParcelableGalleryFilterSection {
    HOT, TOP, USER
}

enum class ParcelableGalleryFilterSort {
    VIRAL, TOP, TIME, RISING
}

enum class ParcelableGalleryFilterWindow {
    DAY, WEEK, MONTH, YEAR, ALL
}

@Parcelize
data class ParcelableGalleryFilter(
    val section: ParcelableGalleryFilterSection = ParcelableGalleryFilterSection.HOT,
    val sort: ParcelableGalleryFilterSort = ParcelableGalleryFilterSort.VIRAL,
    val window: ParcelableGalleryFilterWindow = ParcelableGalleryFilterWindow.DAY,
    val page: Int = 0,
    val showViral: Boolean = true,
    val mature: Boolean = false
) : Parcelable