package com.imgur.feature.gallery.controller

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.*
import com.imgur.core.presentation.gallery.model.PresentationGallery
import com.imgur.core.presentation.gallery.model.PresentationGalleryFilter
import com.imgur.core.presentation.gallery.presenter.GalleryListPresenter
import com.imgur.core.presentation.gallery.view.GalleryDisplayMode
import com.imgur.core.presentation.gallery.view.GalleryListView
import com.imgur.feature.base.ImgurApp
import com.imgur.feature.base.common.PresenterController
import com.imgur.feature.base.util.ViewVisibilityHelper
import com.imgur.feature.base.util.ViewVisibilityHelper.State.*
import com.imgur.feature.gallery.R
import com.imgur.feature.gallery.adapter.GalleryListAdapter
import com.imgur.feature.gallery.createGalleryListModule
import com.imgur.feature.gallery.dialog.FilterDialog
import com.imgur.feature.gallery.galleryCoreModule
import com.imgur.feature.gallery.mapper.ParcelableGalleryFilterMapper
import kotlinx.android.synthetic.main.gallery_controller_list.view.*
import org.rewedigital.katana.KatanaTrait
import org.rewedigital.katana.createComponent
import org.rewedigital.katana.inject
import kotlin.coroutines.experimental.suspendCoroutine

class GalleryListController : PresenterController<GalleryListView, GalleryListPresenter>(), GalleryListView, KatanaTrait {

    override val component = createComponent(
        modules = listOf(createGalleryListModule(
            routerProvider = { parentController?.router ?: router }),
            galleryCoreModule
        ),
        dependsOn = listOf(ImgurApp.applicationComponent, activityContextComponent))

    override val presenter by inject<GalleryListPresenter>()

    private val parcelableGalleryFilter by inject<ParcelableGalleryFilterMapper>()

    private val adapter
        get() = view?.gallery_recyclerView?.adapter as? GalleryListAdapter

    private val linearLayoutManager
        get() = view?.gallery_recyclerView?.layoutManager as? LinearLayoutManager

    private val staggeredGridLayoutManager
        get() = view?.gallery_recyclerView?.layoutManager as? StaggeredGridLayoutManager

    private lateinit var viewVisibilityHelper: ViewVisibilityHelper

    init {
        setHasOptionsMenu(true)
    }

    override fun createView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.gallery_controller_list, container, false).apply {
            applyRenderMode(this, GalleryDisplayMode.GRID)
            gallery_recyclerView.adapter = GalleryListAdapter().apply {
                onGalleryClickListener = {
                    presenter.onGalleryItemClick(it)
                }
            }

            viewVisibilityHelper = ViewVisibilityHelper(contentView = gallery_mainGroup,
                loadingView = offer_loadingAnimation,
                errorView = offer_errorView,
                emptyStateView = offer_emptyState)
        }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        return inflater.inflate(R.menu.gallery_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.gallery_grid -> presenter.onRequestDisplayChange(GalleryDisplayMode.GRID)
            R.id.gallery_list -> presenter.onRequestDisplayChange(GalleryDisplayMode.LIST)
            R.id.gallery_staggered -> presenter.onRequestDisplayChange(GalleryDisplayMode.STAGGERED)
            R.id.gallery_filter -> presenter.onDisplayFilterClick()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun setLoadingAnimationVisible(visible: Boolean) {
        viewVisibilityHelper.state = if (visible) LOADING else CONTENT
    }

    override fun setErrorViewVisible(visible: Boolean) {
        viewVisibilityHelper.state = if (visible) ERROR else CONTENT
    }

    override fun setEmptyViewVisible(visible: Boolean) {
        viewVisibilityHelper.state = if (visible) EMPTY else CONTENT
    }

    override fun setGalleries(items: List<PresentationGallery>) {
        adapter?.items = items
    }

    override fun setRenderMode(mode: GalleryDisplayMode) {
        view?.apply { applyRenderMode(this, mode) }
    }

    private fun applyRenderMode(view: View, mode: GalleryDisplayMode) {
        with(view) {

            // Check if we have a  Linear & Grid LM and return the first visible item
            // If we have the StaggeredGridLayoutManager active, we need to soft cast it and return the
            // first value of the result id
            val firstItem = linearLayoutManager?.findFirstVisibleItemPosition()
                ?: staggeredGridLayoutManager?.findFirstVisibleItemPositions(null)?.first()
                ?: 0

            val columCount = resources.getInteger(R.integer.gallery_grid_columns)

            val lm: RecyclerView.LayoutManager = when (mode) {
                // Grid layout settings
                GalleryDisplayMode.GRID -> GridLayoutManager(context, columCount)

                // List layout settings
                GalleryDisplayMode.LIST -> LinearLayoutManager(context)

                // Staggered layout settings
                GalleryDisplayMode.STAGGERED -> StaggeredGridLayoutManager(columCount, StaggeredGridLayoutManager.VERTICAL)
            }

            gallery_recyclerView.layoutManager = lm
            gallery_recyclerView.recycledViewPool.clear()
            gallery_recyclerView.scrollToPosition(firstItem)

            adapter?.mode = mode
        }
    }

    suspend override fun showFilterView(filter: PresentationGalleryFilter) {
        suspendCoroutine<Boolean> { continuation ->
            (activity as?AppCompatActivity)?.let { activity ->
                FilterDialog.instance(parcelableGalleryFilter.mapTo(filter)).apply {
                    confirmListener = { filter -> presenter.applyFilter(parcelableGalleryFilter.mapFrom(filter)) }

                    show(activity.supportFragmentManager, TAG_DIALOG_FILTER)

//                AlertDialog.Builder(activity).apply {
//                    setTitle(R.string.gallery_filter_title)
//
//                    setMessage(messageStringRes)
//                    setPositiveButton(R.string.favorites_delete) { _, _ ->
//                        continuation.resume(true)
//                    }
//                    setNegativeButton(android.R.string.cancel) { dialog, _ ->
//                        continuation.resume(false)
//                        dialog.dismiss()
//                    }
//                    setCancelable(true)
//                    setOnCancelListener { continuation.resume(false) }
                }
            } ?: continuation.resume(false)
        }
    }

    companion object {
        private val TAG_DIALOG_FILTER = "TAG_DIALOG_FILTER"
    }
}