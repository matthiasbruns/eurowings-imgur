package com.imgur.feature.gallery.decorator

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View
import com.imgur.feature.base.util.Conversions

/**
 * https://stackoverflow.com/a/29168276/982007
 */
class ItemDecorationGalleryColumns(private val sizeGridSpacing: Float, private val gridCount: Int) : RecyclerView.ItemDecoration() {

    private var mNeedLeftSpacing = false

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State?) {
        val sizeGridSpacingPx = Conversions.dpToPx(sizeGridSpacing)

        val frameWidth = ((parent.width - sizeGridSpacingPx.toFloat() * (gridCount - 1)) / gridCount).toInt()
        val padding = parent.width / gridCount - frameWidth
        val itemPosition = (view.layoutParams as RecyclerView.LayoutParams).viewAdapterPosition

        if (itemPosition < gridCount) {
            outRect.top = 0
        } else {
            outRect.top = sizeGridSpacingPx
        }

        if (itemPosition % gridCount == 0) {
            outRect.left = 0
            outRect.right = padding
            mNeedLeftSpacing = true
        } else if ((itemPosition + 1) % gridCount == 0) {
            mNeedLeftSpacing = false
            outRect.right = 0
            outRect.left = padding
        } else if (mNeedLeftSpacing) {
            mNeedLeftSpacing = false
            outRect.left = sizeGridSpacingPx - padding
            if ((itemPosition + 2) % gridCount == 0) {
                outRect.right = sizeGridSpacingPx - padding
            } else {
                outRect.right = sizeGridSpacingPx / 2
            }
        } else if ((itemPosition + 2) % gridCount == 0) {
            mNeedLeftSpacing = false
            outRect.left = sizeGridSpacingPx / 2
            outRect.right = sizeGridSpacingPx - padding
        } else {
            mNeedLeftSpacing = false
            outRect.left = sizeGridSpacingPx / 2
            outRect.right = sizeGridSpacingPx / 2
        }

        outRect.bottom = 0
    }
}