package com.imgur.feature.gallery

import com.imgur.core.domain.gallery.repository.GalleryRemote
import com.imgur.core.domain.gallery.repository.GalleryRepository
import com.imgur.core.domain.gallery.repository.GalleryRepositoryImpl
import com.imgur.core.domain.gallery.usecase.FetchGalleriesUseCase
import com.imgur.core.presentation.gallery.interactor.GalleryListInteractor
import com.imgur.core.presentation.gallery.mapper.PresentationGalleryFilterMapper
import com.imgur.core.presentation.gallery.mapper.PresentationGalleryMapper
import com.imgur.core.presentation.gallery.navigator.GalleryNavigator
import com.imgur.core.presentation.gallery.presenter.GalleryListPresenter
import com.imgur.core.presentation.gallery.presenter.GalleryListPresenterImpl
import com.imgur.core.presentation.gallery.presenter.GalleryListPresenterState
import com.imgur.core.remote.Api
import com.imgur.core.remote.gallery.api.GalleryApi
import com.imgur.core.remote.gallery.mapper.RemoteGalleryFilterMapper
import com.imgur.core.remote.gallery.mapper.RemoteGalleryMapper
import com.imgur.core.remote.gallery.repository.GalleryRemoteImpl
import com.imgur.core.remote.image.mapper.RemoteImageMapper
import com.imgur.feature.base.inject.CREDENTIALS_PROVIDER
import com.imgur.feature.base.inject.RouterProvider
import com.imgur.feature.gallery.mapper.ParcelableGalleryFilterMapper
import com.imgur.feature.gallery.mapper.ParcelableGalleryMapper
import com.imgur.feature.gallery.navigator.ConductorGalleryNavigator
import org.rewedigital.katana.createModule

const val ROUTER_PROVIDER = "ROUTER_PROVIDER"

fun createGalleryDetailModule(routerProvider: RouterProvider) = createModule("createGalleryDetailModule") {

    bind<RouterProvider>(ROUTER_PROVIDER) { singleton { routerProvider } }
}

fun createGalleryListModule(routerProvider: RouterProvider) = createModule("createGalleryListModule") {

    bind<RouterProvider>(ROUTER_PROVIDER) { singleton { routerProvider } }

    bind<GalleryListPresenter> { factory { GalleryListPresenterImpl(get(), get(), get()) } }

    bind<GalleryListPresenterState> { factory { GalleryListPresenterState() } }

    bind<GalleryListInteractor> { singleton { GalleryListInteractor(get(), get(), get()) } }

    bind<PresentationGalleryFilterMapper> { singleton { PresentationGalleryFilterMapper() } }
}

val galleryCoreModule = createModule("galleryCoreModule") {

    bind<PresentationGalleryMapper> { singleton { PresentationGalleryMapper() } }

    bind<FetchGalleriesUseCase> { singleton { FetchGalleriesUseCase(get()) } }

    bind<GalleryRepository> { singleton { GalleryRepositoryImpl(get()) } }

    bind<GalleryRemote> { singleton { GalleryRemoteImpl(get(CREDENTIALS_PROVIDER), get(), get(), get()) } }

    bind<RemoteGalleryMapper> { singleton { RemoteGalleryMapper(get()) } }

    bind<RemoteImageMapper> { singleton { RemoteImageMapper() } }

    bind<GalleryApi> { factory { get<Api>().gallery } }

    bind<RemoteGalleryFilterMapper> { singleton { RemoteGalleryFilterMapper() } }

    bind<ParcelableGalleryFilterMapper> { singleton { ParcelableGalleryFilterMapper() } }

    bind<ParcelableGalleryMapper> { singleton { ParcelableGalleryMapper() } }

    bind<GalleryNavigator> {
        // Choose navigator based on instant app or installed version
        singleton { ConductorGalleryNavigator(get(ROUTER_PROVIDER), get()) }
    }
}