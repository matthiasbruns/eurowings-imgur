package com.imgur.feature.gallery.mapper

import com.imgur.core.common.mapper.BidirectionalMapper
import com.imgur.core.presentation.gallery.model.PresentationGallery
import com.imgur.feature.gallery.model.ParcelableGallery

class ParcelableGalleryMapper : BidirectionalMapper<PresentationGallery, ParcelableGallery> {

    override fun mapFrom(to: ParcelableGallery) =
        PresentationGallery(
            id = to.id,
            coverUrl = to.coverUrl,
            description = to.description,
            title = to.title,
            score = to.score,
            upvotes = to.upvotes,
            downvotes = to.downvotes
        )

    override fun mapTo(from: PresentationGallery) =
        ParcelableGallery(
            id = from.id,
            coverUrl = from.coverUrl,
            description = from.description,
            title = from.title,
            score = from.score,
            upvotes = from.upvotes,
            downvotes = from.downvotes
        )
}