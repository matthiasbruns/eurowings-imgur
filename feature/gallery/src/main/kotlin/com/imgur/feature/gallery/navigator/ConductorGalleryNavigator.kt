package com.imgur.feature.gallery.navigator

import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.VerticalChangeHandler
import com.imgur.core.presentation.gallery.model.PresentationGallery
import com.imgur.core.presentation.gallery.navigator.GalleryNavigator
import com.imgur.feature.base.inject.RouterProvider
import com.imgur.feature.gallery.controller.GalleryDetailController
import com.imgur.feature.gallery.mapper.ParcelableGalleryMapper

class ConductorGalleryNavigator(private val routerProvider: RouterProvider,
                                private val mapper: ParcelableGalleryMapper) : GalleryNavigator {

    override fun openDetails(gallery: PresentationGallery) {
        routerProvider()?.apply {
            mapper.mapTo(gallery).also {
                val controller = GalleryDetailController.instance(it)
                routerProvider()?.pushController(RouterTransaction.with(controller)
                    .pushChangeHandler(VerticalChangeHandler())
                    .popChangeHandler(VerticalChangeHandler()))
            }
        }
    }
}