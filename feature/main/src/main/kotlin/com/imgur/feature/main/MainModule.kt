package com.imgur.feature.main

import com.google.android.instantapps.InstantApps
import com.imgur.core.common.android.inject.ContextProvider
import com.imgur.core.presentation.main.navigator.MainNavigator
import com.imgur.core.presentation.main.presenter.MainPresenter
import com.imgur.core.presentation.main.presenter.MainPresenterImpl
import com.imgur.core.presentation.main.presenter.MainPresenterState
import com.imgur.feature.base.inject.ACTIVITY_CONTEXT_PROVIDER
import com.imgur.feature.base.inject.RouterProvider
import com.imgur.feature.main.navigator.MainConductorNavigator
import com.imgur.feature.main.navigator.MainInstantAppNavigator
import org.rewedigital.katana.createModule

const val ROOT_ROUTER_PROVIDER = "ROOT_ROUTER_PROVIDER"
const val ROUTER_PROVIDER = "ROUTER_PROVIDER"

fun createMainModule(rootRouterProvider: RouterProvider,
                     routerProvider: RouterProvider) = createModule("mainModule") {

    bind<RouterProvider>(ROUTER_PROVIDER) { singleton { routerProvider } }

    bind<RouterProvider>(ROOT_ROUTER_PROVIDER) { singleton { rootRouterProvider } }

    bind<MainPresenter> { factory { MainPresenterImpl(state = get(), mainNavigator = get()) } }

    bind<MainPresenterState> { factory { MainPresenterState() } }

    bind<MainNavigator> {
        factory {
            if (InstantApps.isInstantApp(get<ContextProvider>(ACTIVITY_CONTEXT_PROVIDER)()!!)) {
                MainInstantAppNavigator(get(ACTIVITY_CONTEXT_PROVIDER))
            } else {
                MainConductorNavigator(get(ACTIVITY_CONTEXT_PROVIDER), get(ROUTER_PROVIDER))
            }
        }
    }
}

val mainCoreModule = createModule("mainCoreModule") {

}