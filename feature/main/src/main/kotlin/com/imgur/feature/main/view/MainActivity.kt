package com.imgur.feature.main.view

import com.bluelinelabs.conductor.Controller
import com.imgur.feature.base.common.ControllerActivity
import com.imgur.feature.main.controller.MainController

class MainActivity : ControllerActivity() {

    override fun provideController(): Controller = MainController()
}