package com.imgur.feature.main.controller

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.imgur.core.presentation.main.presenter.MainPresenter
import com.imgur.core.presentation.main.view.MainView
import com.imgur.feature.base.ImgurApp
import com.imgur.feature.base.common.PresenterController
import com.imgur.feature.main.R
import com.imgur.feature.main.createMainModule
import com.imgur.feature.main.mainCoreModule
import kotlinx.android.synthetic.main.main_controller_main.view.*
import org.jetbrains.anko.find
import org.jetbrains.anko.findOptional
import org.rewedigital.katana.KatanaTrait
import org.rewedigital.katana.createComponent
import org.rewedigital.katana.inject

class MainController : PresenterController<MainView, MainPresenter>(),
    MainView,
    KatanaTrait {

    private val routerProvider = {
        view?.find<ViewGroup>(R.id.main_childControllerContainer)?.let { container ->
            getChildRouter(container)
        }
    }

    private val rootRouterProvider = { router }

    override val component =
        createComponent(
            modules = listOf(createMainModule(
                rootRouterProvider = rootRouterProvider,
                routerProvider = routerProvider),
                mainCoreModule
            ),
            dependsOn = listOf(ImgurApp.applicationComponent, activityContextComponent))

    override val presenter by inject<MainPresenter>()

    override fun onAttach(view: View) {
        view.findOptional<Toolbar>(com.imgur.feature.base.R.id.toolbar)?.let { toolbar ->
            (activity as? AppCompatActivity)?.apply {
                setSupportActionBar(toolbar)
                supportActionBar?.setDisplayShowTitleEnabled(false)
            }
        }
        super.onAttach(view)
    }

    override fun createView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.main_controller_main, container, false).also { view ->
            with(view.main_navigationView) {
                listener = { itemId ->
                    when (itemId) {
                        R.id.main_nav_gallery -> presenter.onGalleryNavItemClick()
                        R.id.main_nav_profile -> presenter.onProfileNavItemClick()
                        R.id.main_nav_settings -> presenter.onSettingsNavItemClick()
                    }
                }
            }
        }
}