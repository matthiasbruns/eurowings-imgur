package com.imgur.feature.main.view

import android.annotation.SuppressLint
import android.content.Context
import android.support.design.internal.BottomNavigationItemView
import android.support.design.internal.BottomNavigationMenuView
import android.util.AttributeSet
import com.imgur.core.common.extentions.log
import com.imgur.core.common.log.Logger
import com.imgur.feature.main.R

typealias ItemSelectedListener = (Int) -> Unit

class BottomNavigationView @JvmOverloads constructor(context: Context,
                                                     attrs: AttributeSet? = null,
                                                     defStyleAttr: Int = 0) :
        android.support.design.widget.BottomNavigationView(context, attrs, defStyleAttr) {

    var listener: ItemSelectedListener? = null

    init {
        setBackgroundResource(R.drawable.main_bottom_navigation_background)
        inflateMenu(R.menu.main_bottom_navigation)
        setOnNavigationItemSelectedListener { item -> listener?.let { it(item.itemId) }; true }
        disableShiftingMode()
    }

    @SuppressLint("RestrictedApi")
    private fun disableShiftingMode() =
            try {
                val menu = getChildAt(0) as BottomNavigationMenuView
                val shiftingModeField = menu.javaClass.getDeclaredField("mShiftingMode")
                shiftingModeField.isAccessible = true
                shiftingModeField.setBoolean(menu, false)
                shiftingModeField.isAccessible = false
                for (i in 0 until menu.childCount) {
                    val item = menu.getChildAt(i) as BottomNavigationItemView
                    item.setShiftingMode(false)
                    item.setChecked(item.itemData.isChecked)
                }
            } catch (e: Exception) {
                e.log(level = Logger.Level.WARN, message = "Couldn't disable shifting mode")
            }
}
