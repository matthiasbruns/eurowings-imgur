package com.imgur.feature.main.navigator

import android.widget.Toast
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.RouterTransaction
import com.imgur.core.common.android.inject.ContextProvider
import com.imgur.core.presentation.main.navigator.MainNavigator
import com.imgur.feature.base.inject.RouterProvider
import com.imgur.feature.gallery.controller.GalleryListController
import com.imgur.feature.main.R
import com.imgur.feature.settings.controller.SettingsController

class MainConductorNavigator(private val contextProvider: ContextProvider,
                             private val routerProvider: RouterProvider) : MainNavigator {

    private enum class NavigationItem(val controllerProvider: () -> Controller) {
        GALLERY(::GalleryListController),
        SETTINGS(::SettingsController)
    }

    private var currentNavigationItem: NavigationItem? = null

    override fun navigateToGallery() {
        navigateTo(NavigationItem.GALLERY)
    }

    override fun navigateToProfile() = printMissingFeature()

    override fun navigateToSettings() = navigateTo(NavigationItem.SETTINGS)

    private fun printMissingFeature() {
        contextProvider()?.apply {
            Toast.makeText(this, getString(R.string.main_feature_missing), Toast.LENGTH_SHORT).show()
        }
    }

    private fun navigateTo(navigationItem: NavigationItem) {
        if (currentNavigationItem == navigationItem) return
        currentNavigationItem = navigationItem
        routerProvider()?.replaceTopController(RouterTransaction.with(navigationItem.controllerProvider()))
    }
}
