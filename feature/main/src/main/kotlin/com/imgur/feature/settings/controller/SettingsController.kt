package com.imgur.feature.settings.controller

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bluelinelabs.conductor.Controller
import com.imgur.feature.main.R
import kotlinx.android.synthetic.main.settings_controller.view.*
import java.text.SimpleDateFormat
import java.util.*


/**
 * This controller and all classes in com.imgur.feature.settings belong into its own feature as the "gallery" classes.
 * But this is out of scope.
 */
class SettingsController : Controller() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.settings_controller, container, false).apply {
            activity?.also { context ->
                try {
                    val packageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
                    settings_deployment.text = Date(packageInfo.lastUpdateTime).let { SimpleDateFormat.getDateTimeInstance().format(it) }
                } catch (e: Exception) {
                    e.printStackTrace()
                    settings_deployment.visibility = View.GONE
                }
            }
        }
}