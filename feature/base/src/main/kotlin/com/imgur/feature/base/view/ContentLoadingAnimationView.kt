package com.imgur.feature.base.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch

/**
 * See [android.support.v4.widget.ContentLoadingProgressBar] for details ;)
 */
class ContentLoadingAnimationView @JvmOverloads constructor(context: Context,
                                                            attrs: AttributeSet? = null,
                                                            defStyleAttr: Int = 0) :
    LoadingAnimationView(context, attrs, defStyleAttr) {

    private var startTime = -1L
    private var showJob: Job? = null
    private var hideJob: Job? = null

    init {
        visibility = View.GONE
    }

    @Synchronized
    fun show(onShow: (() -> Unit)? = null) {
        cancelHide()
        if (visibility == View.VISIBLE) {
            onShow?.invoke()
            return
        }
        if (showJob != null) return
        startTime = -1L
        showJob = launch(UI) {
            delay(MIN_DELAY_MS)
            startTime = System.currentTimeMillis()
            visibility = View.VISIBLE
            showJob = null
            onShow?.invoke()
        }
    }

    @Synchronized
    fun hide(onHide: (() -> Unit)? = null) {
        cancelShow()
        val diff = System.currentTimeMillis() - startTime
        if (diff >= MIN_SHOW_TIME_MS || startTime == -1L) {
            visibility = View.GONE
            onHide?.invoke()
        } else if (hideJob == null) {
            hideJob = launch(UI) {
                delay(MIN_SHOW_TIME_MS - diff)
                startTime = -1L
                visibility = View.GONE
                hideJob = null
                onHide?.invoke()
            }
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        cancelShow()
        cancelHide()
    }

    override fun onDetachedFromWindow() {
        cancelShow()
        cancelHide()
        super.onDetachedFromWindow()
    }

    private fun cancelShow() {
        showJob?.cancel()
        showJob = null
    }

    private fun cancelHide() {
        hideJob?.cancel()
        hideJob = null
    }

    private companion object {
        private const val MIN_DELAY_MS = 500
        private const val MIN_SHOW_TIME_MS = 500
    }
}
