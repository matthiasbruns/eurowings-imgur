package com.imgur.feature.base.view

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView

interface AutoUpdatableAdapterTrait {

    fun <T> RecyclerView.Adapter<*>.autoNotify(oldList: List<T>, newList: List<T>,
                                               areContentsTheSame: (old: T, new: T) -> Boolean = { old, new -> old == new },
                                               areItemsTheSame: (old: T, new: T) -> Boolean) {
        val diff = DiffUtil.calculateDiff(object : DiffUtil.Callback() {

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                areItemsTheSame(oldList[oldItemPosition], newList[newItemPosition])

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                areContentsTheSame(oldList[oldItemPosition], newList[newItemPosition])

            override fun getOldListSize() =
                oldList.size

            override fun getNewListSize() =
                newList.size
        })

        diff.dispatchUpdatesTo(this)
    }
}
