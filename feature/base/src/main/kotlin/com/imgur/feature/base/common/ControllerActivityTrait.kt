package com.imgur.feature.base.common

import android.os.Bundle
import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import org.jetbrains.anko.find

/**
 * Trait that holds implementations for Activities which contain at least one Conductor [Controller].
 *
 * Implementations must call [onCreateDelegate] in `onCreate()` and [onBackPressedDelegate] in `onBackPressed()`.
 */
interface ControllerActivityTrait : BaseActivityTrait {

    @get:LayoutRes val layoutResId: Int
    @get:IdRes val containerId: Int

    var router: Router

    fun provideController(): Controller

    fun onCreateDelegate(savedInstanceState: Bundle?) {
        activity.setContentView(layoutResId)

        router = Conductor.attachRouter(activity, activity.find(containerId), savedInstanceState)
        if (!router.hasRootController()) {
            router.setRoot(RouterTransaction.with(provideController()))
        }
    }

    fun onBackPressedDelegate() = router.handleBack()
}
