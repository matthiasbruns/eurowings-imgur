package com.imgur.feature.base.common
import android.support.v7.app.AppCompatActivity

interface BaseActivityTrait {

    val activity: AppCompatActivity
}
