package com.imgur.feature.base.util

import android.content.res.Resources

object Conversions {

    fun dpToPx(dp: Number) =
        (dp.toInt() * Resources.getSystem().displayMetrics.density).toInt()

    fun pxToDp(px: Number) =
        (px.toInt() / Resources.getSystem().displayMetrics.density).toInt()
}
