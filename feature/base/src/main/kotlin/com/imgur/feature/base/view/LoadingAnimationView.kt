package com.imgur.feature.base.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import com.airbnb.lottie.LottieAnimationView
import com.airbnb.lottie.LottieDrawable
import com.imgur.feature.base.R

open class LoadingAnimationView @JvmOverloads constructor(context: Context,
                                                          attrs: AttributeSet? = null,
                                                          defStyleAttr: Int = 0) :
    LottieAnimationView(context, attrs, defStyleAttr) {

    private var isPlaying = false

    init {
        this.enableMergePathsForKitKatAndAbove(true)
        this.setAnimation(R.raw.loading)
        repeatCount = LottieDrawable.INFINITE
    }

    override fun setVisibility(visibility: Int) {
        super.setVisibility(visibility)

        if (visibility == View.VISIBLE || !isPlaying) {
            isPlaying = true
            playAnimation()
        }
    }
}
