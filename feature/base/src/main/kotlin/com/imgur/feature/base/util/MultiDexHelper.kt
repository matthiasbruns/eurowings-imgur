package com.imgur.feature.base.util

import android.content.Context

interface MultiDexHelper {

    fun install(context: Context)
}
