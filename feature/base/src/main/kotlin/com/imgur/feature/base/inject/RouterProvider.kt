package com.imgur.feature.base.inject

import com.bluelinelabs.conductor.Router

typealias RouterProvider = () -> Router?