package com.imgur.feature.base.view

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import com.imgur.feature.base.R
import kotlinx.android.synthetic.main.view_empty_state.view.*

class EmptyStateView @JvmOverloads constructor(context: Context,
                                               attrs: AttributeSet? = null,
                                               defStyleAttr: Int = 0) : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        inflate(context, R.layout.view_empty_state, this)

        attrs?.let {
            with(context.obtainStyledAttributes(it, R.styleable.EmptyStateView, defStyleAttr, 0)) {
                emptyState_text.text = getText(R.styleable.EmptyStateView_android_text)
                if (hasValue(R.styleable.EmptyStateView_srcCompat)) {
                    emptyState_image.setImageDrawable(getDrawable(R.styleable.EmptyStateView_srcCompat))
                }
                recycle()
            }
        }
    }
}
