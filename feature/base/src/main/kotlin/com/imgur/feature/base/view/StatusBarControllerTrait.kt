package com.imgur.feature.base.view

import android.annotation.SuppressLint
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.view.View
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.ControllerChangeHandler
import com.bluelinelabs.conductor.ControllerChangeType
import com.imgur.core.common.android.extensions.runOn23Marshmallow

interface StatusBarControllerTrait {

    val controller: Controller

    fun onChangeStartedDelegate(changeHandler: ControllerChangeHandler, changeType: ControllerChangeType) {
        // Reset to default bar when leaving this view
        if (changeType == ControllerChangeType.POP_EXIT || changeType == ControllerChangeType.PUSH_EXIT) controller.resetStatusBar()
    }

    fun onChangeEndedDelegate(changeHandler: ControllerChangeHandler, changeType: ControllerChangeType) {
        // Set white status bar when entered view
        if (changeType == ControllerChangeType.POP_ENTER || changeType == ControllerChangeType.PUSH_ENTER) controller.enableWhiteStatusBar()
    }

    @SuppressLint("InlinedApi")
    fun Controller.enableWhiteStatusBar() {
        runOn23Marshmallow {
            var flags = view?.systemUiVisibility ?: 0
            flags = flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            view?.systemUiVisibility = flags
            activity?.window?.statusBarColor = Color.WHITE
        }
    }

    @SuppressLint("InlinedApi")
    fun Controller.resetStatusBar() {
        activity?.apply {
            runOn23Marshmallow {
                var flags = view?.systemUiVisibility ?: 0
                flags = flags and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
                view?.systemUiVisibility = flags
                window?.statusBarColor = ContextCompat.getColor(this, com.imgur.feature.base.R.color.color_primary)
            }
        }

    }
}
