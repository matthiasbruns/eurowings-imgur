package com.imgur.feature.base.view

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import com.imgur.feature.base.R
import kotlinx.android.synthetic.main.view_error.view.*

class ErrorView @JvmOverloads constructor(context: Context,
                                          attrs: AttributeSet? = null,
                                          defStyleAttr: Int = 0) : ConstraintLayout(context, attrs, defStyleAttr) {

    var onRetryClick: (() -> Unit)? = null

    init {
        inflate(context, R.layout.view_error, this)

        attrs?.let {
            with(context.obtainStyledAttributes(it, R.styleable.ErrorView, defStyleAttr, 0)) {
                error_descriptionText.text = getText(R.styleable.ErrorView_android_text)
                if (hasValue(R.styleable.ErrorView_srcCompat)) {
                    error_imageView.setImageDrawable(getDrawable(R.styleable.EmptyStateView_srcCompat))
                }
                recycle()
            }
        }

        error_retryButton.setOnClickListener { onRetryClick?.invoke() }
    }
}
