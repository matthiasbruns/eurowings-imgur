package com.imgur.feature.base.inject

import com.imgur.core.common.log.Logger
import org.rewedigital.katana.Katana

object KatanaLogger : Katana.Logger {

    override fun debug(msg: String) {
        Logger.d("[KATANA] $msg")
    }

    override fun info(msg: String) {
        Logger.i("[KATANA] $msg")
    }

    override fun warn(msg: String) {
        Logger.w("[KATANA] $msg")
    }

    override fun error(msg: String, throwable: Throwable?) {
        Logger.e(throwable, "[KATANA] $msg")
    }
}
