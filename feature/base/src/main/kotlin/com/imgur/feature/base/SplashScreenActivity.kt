package com.imgur.feature.base

import com.imgur.feature.base.common.BaseActivityTrait

/**
 * Resets application theme to the default theme replacing the splash theme which has been defined in
 * AndroidManifest.xml. [onCreateDelegate] must be called in `onCreate()` *before* the super call!
 */
interface SplashScreenActivityTrait : BaseActivityTrait {

    /**
     * Must be called in `onCreate()` *before* the super call!
     */
    fun onCreateDelegate() {
        activity.setTheme(R.style.AppTheme)
    }
}
