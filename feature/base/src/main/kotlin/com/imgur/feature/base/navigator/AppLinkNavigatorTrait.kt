package com.imgur.feature.base.navigator

import android.content.Intent
import android.net.Uri

interface AppLinkNavigatorTrait {

    fun createAppLinkIntent(path: String): Intent =
        Intent(Intent.ACTION_VIEW, Uri.parse("https://imgur.com/app/$path")).apply {
            addCategory(Intent.CATEGORY_BROWSABLE)
        }
}
