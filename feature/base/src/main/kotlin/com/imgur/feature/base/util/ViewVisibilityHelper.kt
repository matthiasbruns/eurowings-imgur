package com.imgur.feature.base.util

import android.view.View
import com.imgur.feature.base.view.ContentLoadingAnimationView

class ViewVisibilityHelper(private val contentView: View?,
                           private val loadingView: ContentLoadingAnimationView? = null,
                           private val errorView: View? = null,
                           private val emptyStateView: View? = null) {

    var onContentVisibilityChanged: ((visibility: Int) -> Unit)? = null

    enum class State { CONTENT, LOADING, ERROR, EMPTY }

    var state: State = State.LOADING
        set(value) {
            field = value
            when (state) {
                State.CONTENT -> {
                    hideLoadingView {
                        setContentViewVisibility(View.VISIBLE)
                    }
                    errorView?.visibility = View.GONE
                    emptyStateView?.visibility = View.GONE
                }
                State.LOADING -> {
                    setContentViewVisibility(View.GONE)
                    errorView?.visibility = View.GONE
                    emptyStateView?.visibility = View.GONE
                    loadingView?.show()
                }
                State.ERROR -> {
                    hideLoadingView {
                        errorView?.visibility = View.VISIBLE
                    }
                    setContentViewVisibility(View.GONE)
                    emptyStateView?.visibility = View.GONE
                }
                State.EMPTY -> {
                    hideLoadingView {
                        emptyStateView?.visibility = View.VISIBLE
                    }
                    setContentViewVisibility(View.GONE)
                    errorView?.visibility = View.GONE
                }
            }
        }

    private fun hideLoadingView(onHide: () -> Unit) =
        if (loadingView == null) onHide() else loadingView.hide(onHide)

    private fun setContentViewVisibility(visibility: Int) {
        contentView?.visibility = visibility
        onContentVisibilityChanged?.invoke(visibility)
    }
}
