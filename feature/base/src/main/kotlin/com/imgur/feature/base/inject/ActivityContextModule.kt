package com.imgur.feature.base.inject

import com.imgur.core.common.android.inject.ActivityProvider
import com.imgur.core.common.android.inject.ContextProvider
import org.rewedigital.katana.createModule

const val ACTIVITY_CONTEXT_PROVIDER = "ACTIVITY_CONTEXT_PROVIDER"
const val ACTIVITY_PROVIDER = "ACTIVITY_PROVIDER"

/**
 * Creates a Katana module which provides the current Activity's [android.content.Context] via a [ContextProvider].
 * The [ContextProvider] is bound to the name ACTIVITY_CONTEXT_PROVIDER.
 *
 * The module is created in the constructor of [com.imgur.feature.base.common.base.PresenterController].
 */
fun createActivityContextModule(activityProvider: ActivityProvider) = createModule("activityContextModule") {

    bind<ContextProvider>(ACTIVITY_CONTEXT_PROVIDER) { singleton { { activityProvider() } } }

    bind<ActivityProvider>(ACTIVITY_PROVIDER) { singleton { activityProvider } }
}
