package com.imgur.feature.base.common.log

import android.annotation.SuppressLint
import com.imgur.core.common.extentions.emptyToNull
import com.imgur.core.common.log.Logger
import com.imgur.core.common.log.LoggerAdapter
import timber.log.Timber
import timber.log.Timber.DebugTree


@SuppressLint("TimberExceptionLogging")
class TimberLoggerAdapter : LoggerAdapter {

    init {
        Timber.plant(DebugTree())
    }

    private val explicitTag = ThreadLocal<String?>()

    override fun v(throwable: Throwable?, message: String?, vararg args: Any?) {
        applyTag()
        Timber.v(throwable, message, *args)
    }

    override fun d(throwable: Throwable?, message: String?, vararg args: Any?) {
        applyTag()
        Timber.d(throwable, message, *args)
    }

    override fun i(throwable: Throwable?, message: String?, vararg args: Any?) {
        applyTag()
        Timber.i(throwable, message, *args)
    }

    override fun w(throwable: Throwable?, message: String?, vararg args: Any?) {
        applyTag()
        Timber.w(throwable, message, *args)
    }

    override fun e(throwable: Throwable?, message: String?, vararg args: Any?) {
        applyTag()
        Timber.e(throwable, message, *args)
    }

    override fun tag(tag: String?) {
        explicitTag.set(tag)
    }

    private fun applyTag() {
        val tag = explicitTag.get()?.apply { explicitTag.remove() } ?: findTag()
        if (tag != null) Timber.tag(tag)
    }

    private fun findTag() =
        try {
            val className = Thread.currentThread().stackTrace.find { element ->
                IGNORED_TAG_CLASSES.contains(element.className).not()
            }?.className
            className?.withoutAnonymous?.let { Class.forName(it).simpleName }?.emptyToNull()
        } catch (e: Exception) {
            null
        }

    private val String.withoutAnonymous
        get() = substringBefore("$")

    private companion object {

        private val IGNORED_TAG_CLASSES = listOf(
            TimberLoggerAdapter::class.qualifiedName,
            LoggerAdapter::class.qualifiedName,
            Logger::class.qualifiedName,
            Thread::class.qualifiedName,
            "dalvik.system.VMStack"
        )
    }
}
