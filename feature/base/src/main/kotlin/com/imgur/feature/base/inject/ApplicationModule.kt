package com.imgur.feature.base.inject

import android.content.Context
import com.imgur.core.common.android.inject.ContextProvider
import com.imgur.core.remote.Api
import com.imgur.core.remote.ApiCredentials
import com.imgur.core.remote.ApiFactory
import com.imgur.core.remote.CredentialProvider
import com.imgur.feature.base.ImgurApp
import org.rewedigital.katana.createModule

const val APP_CONTEXT = "APP_CONTEXT"
const val APP_CONTEXT_PROVIDER = "APP_CONTEXT_PROVIDER"
const val CREDENTIALS_PROVIDER = "CREDENTIALS_PROVIDER"

fun createApplicationModule(app: ImgurApp) = createModule("applicationModule") {

    bind<Context>(APP_CONTEXT) { singleton { app.applicationContext } }

    bind<ContextProvider>(APP_CONTEXT_PROVIDER) { singleton { { app.applicationContext } } }
}

fun createRetrofitModule() = createModule("createRetrofitModule") {

    bind<CredentialProvider>(CREDENTIALS_PROVIDER) {
        singleton {
            // This needs to be placed e.g. in the AccountProvider api or some secret place
            // The credentials could be connected to the build type and can be swapped here
            // You could also provide a RunConfig as a required dependency for this injection to enable runtime DEV/PRE/PROD swaps
            { ApiCredentials(clienId = "a62f7a607c5ef2e", clientSecret = null) }
        }
    }

    bind<Api> { singleton { ApiFactory(get(CREDENTIALS_PROVIDER)).create() } }
}