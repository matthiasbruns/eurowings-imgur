package com.imgur.feature.base

import android.app.Application
import android.content.Context
import android.os.StrictMode
import com.imgur.core.common.log.Logger
import com.imgur.feature.base.common.log.TimberLoggerAdapter
import com.imgur.feature.base.inject.createApplicationModule
import com.imgur.feature.base.inject.createRetrofitModule
import com.imgur.feature.base.util.MultiDexHelperImpl
import org.rewedigital.katana.Component
import org.rewedigital.katana.createComponent

class ImgurApp : Application() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDexHelperImpl.install(this)
    }

    override fun onCreate() {
        super.onCreate()

        setupStrictMode()
        setupLogger()

        applicationComponent = createComponent(modules = listOf(createApplicationModule(this),
            createRetrofitModule()))
    }


    private fun setupStrictMode() {
        if (BuildConfig.DEBUG) return

        Logger.d("Enabling Android's strict mode")

        StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder()
            .detectNetwork()
            .detectCustomSlowCalls()
            .penaltyLog()
            .penaltyDeath()
            .build())

        StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder()
            .detectActivityLeaks()
            .detectLeakedClosableObjects()
            .penaltyLog()
            .build())
    }

    private fun setupLogger() {
        Logger.adapter = TimberLoggerAdapter()
    }

    companion object {
        lateinit var applicationComponent: Component
    }
}