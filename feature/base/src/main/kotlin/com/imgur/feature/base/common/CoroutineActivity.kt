package com.imgur.feature.base.common

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.imgur.core.common.coroutine.CoroutineContextHolder
import com.imgur.feature.base.SplashScreenActivityTrait
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI

/**
 * Base class for Activities which hold a coroutine context
 * that is bound to the lifecycle of the Activity.
 *
 * @see CoroutineContextHolder
 */
abstract class CoroutineActivity : AppCompatActivity(),
        SplashScreenActivityTrait,
        CoroutineContextHolder {

    private val job = Job()
    override val coroutineContext = job + UI

    override val activity
        get() = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreateDelegate()
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        job.cancel()
        super.onDestroy()
    }
}
