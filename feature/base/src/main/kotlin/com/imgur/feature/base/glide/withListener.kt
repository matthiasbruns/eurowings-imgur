package com.imgur.feature.base.glide

import android.graphics.drawable.Drawable
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory

fun <T> RequestBuilder<T>.withListener(onLoadFailed: () -> Boolean = { false },
                                       onResourceReady: () -> Boolean) =
    listener(object : RequestListener<T> {
        override fun onLoadFailed(e: GlideException?,
                                  model: Any,
                                  target: Target<T>,
                                  isFirstResource: Boolean) =
            onLoadFailed()

        override fun onResourceReady(resource: T,
                                     model: Any,
                                     target: Target<T>,
                                     dataSource: DataSource,
                                     isFirstResource: Boolean) =
            onResourceReady()
    })

// Enabling cross fade transition with transparent placeholder images might lead to unpleasant effects. In this case
// set crossFadeEnabled to true. See https://bumptech.github.io/glide/doc/transitions.html#cross-fading-with-placeholders-and-transparent-images
// for details.
fun GlideRequest<Drawable>.withCrossFadeTransition(crossFadeEnabled: Boolean = false) =
    transition(DrawableTransitionOptions.withCrossFade(DrawableCrossFadeFactory.Builder()
        .setCrossFadeEnabled(crossFadeEnabled)
        .build()))
