package com.imgur.feature.base.util

import android.content.Context
import android.support.multidex.MultiDex

object MultiDexHelperImpl : MultiDexHelper {

    override fun install(context: Context) {
        MultiDex.install(context)
    }
}
