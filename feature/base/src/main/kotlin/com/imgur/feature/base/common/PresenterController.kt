package com.imgur.feature.base.common

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bluelinelabs.conductor.Controller
import com.imgur.core.common.coroutine.BgCoroutineContextHolder
import com.imgur.core.common.coroutine.UiCoroutineContextHolder
import com.imgur.core.presentation.base.BasePresenter
import com.imgur.core.presentation.base.BaseView
import com.imgur.feature.base.inject.createActivityContextModule
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.cancel
import org.rewedigital.katana.createComponent
import kotlin.coroutines.experimental.CoroutineContext

/**
 * Base class for Conductor [Controller] that represents the V (view) in MVP.
 */
abstract class PresenterController<VIEW : BaseView, out P : BasePresenter<VIEW>>(bundle: Bundle? = null) :
        Controller(bundle),
        UiCoroutineContextHolder,
        BgCoroutineContextHolder {

    val activityContextComponent = createComponent(createActivityContextModule { activity as? AppCompatActivity })

    init {
        retainViewMode = RetainViewMode.RETAIN_DETACH
    }

    override var uiCoroutineContext: CoroutineContext? = null
    override val bgCoroutineContext: CoroutineContext = CommonPool + Job()

    abstract val presenter: P

    private var onCreateCalled = false

    /**
     * Implementations must inflate the Android [View] here.
     */
    abstract fun createView(inflater: LayoutInflater, container: ViewGroup): View

    @Suppress("UNCHECKED_CAST")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup) =
            createView(inflater, container)
                    .also {
                        uiCoroutineContext = UI + Job()
                        presenter.view = this@PresenterController as VIEW
                        if (!onCreateCalled) {
                            onCreateCalled = true
                            presenter.onCreate()
                        }
                    }

    override fun onDestroyView(view: View) {
        presenter.view = null
        uiCoroutineContext?.cancel()
        uiCoroutineContext = null
        super.onDestroyView(view)
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        presenter.onStart()
    }

    override fun onDetach(view: View) {
        presenter.onStop()
        super.onDetach(view)
    }

    override fun onDestroy() {
        presenter.onDestroy()
        bgCoroutineContext.cancel()
        super.onDestroy()
    }
}
