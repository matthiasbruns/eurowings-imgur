package com.imgur.feature.base.view

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.View
import com.imgur.feature.base.R
import kotlinx.android.synthetic.main.view_imgur_button.view.*

class ImgurButton @JvmOverloads constructor(context: Context,
                                            attrs: AttributeSet? = null,
                                            defStyleAttr: Int = 0) : ConstraintLayout(context, attrs, defStyleAttr) {

    var text: CharSequence?
        get() = imgurButton_text.text
        set(value) {
            imgurButton_text.text = value
        }

    init {
        inflate(context, R.layout.view_imgur_button, this)

        attrs?.let {
            with(context.obtainStyledAttributes(it, R.styleable.ImgurButton, defStyleAttr, 0)) {
                imgurButton_text.text = getText(R.styleable.ImgurButton_android_text)

                if (hasValue(R.styleable.ImgurButton_android_drawableLeft)) {
                    imgurButton_iconLeft.setImageDrawable(getDrawable(R.styleable.ImgurButton_android_drawableLeft))
                    imgurButton_iconLeft.visibility = View.VISIBLE
                } else {
                    imgurButton_iconLeft.visibility = View.GONE
                }

                recycle()
            }
        }
    }
}
