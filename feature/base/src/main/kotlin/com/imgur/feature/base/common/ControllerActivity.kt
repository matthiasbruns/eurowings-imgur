package com.imgur.feature.base.common

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.Router
import com.imgur.core.common.android.permission.RequestPermissionResultTrait
import com.imgur.core.common.android.permission.RequestPermissionsResultHandler
import com.imgur.feature.base.R
import com.imgur.feature.base.SplashScreenActivityTrait

/**
 * Base class for an Activity which contains Conductor [Controller].
 *
 * @see ControllerActivityTrait
 */
abstract class ControllerActivity : AppCompatActivity(),
        ControllerActivityTrait,
        SplashScreenActivityTrait,
        RequestPermissionResultTrait {

    override val layoutResId = R.layout.activity_controller
    override val containerId = R.id.controller_container
    override var permissionResultHandler: RequestPermissionsResultHandler? = null
    override val activity
        get() = this
    override lateinit var router: Router

    override fun onCreateDelegate() {
        super<SplashScreenActivityTrait>.onCreateDelegate()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        onCreateDelegate()
        super.onCreate(savedInstanceState)
        super<ControllerActivityTrait>.onCreateDelegate(savedInstanceState)
    }

    override fun onBackPressed() {
        if (!super.onBackPressedDelegate()) {
            super.onBackPressed()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        onRequestPermissionsResultDelegate(requestCode, permissions, grantResults)
    }
}
