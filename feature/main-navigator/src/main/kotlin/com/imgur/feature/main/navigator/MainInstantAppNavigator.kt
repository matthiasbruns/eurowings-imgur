package com.imgur.feature.main.navigator

import com.imgur.core.common.android.inject.ContextProvider
import com.imgur.core.presentation.main.navigator.MainNavigator
import com.imgur.feature.base.navigator.AppLinkNavigatorTrait

class MainInstantAppNavigator(private val contextProvider: ContextProvider) : MainNavigator,
    AppLinkNavigatorTrait {

    override fun navigateToGallery() {
        val intent = createAppLinkIntent("gallery")
        contextProvider()?.startActivity(intent)
    }

    override fun navigateToProfile() {
        TODO("Implement for Instant App support")
    }

    override fun navigateToSettings() {
        TODO("Implement for Instant App support")
    }
}
